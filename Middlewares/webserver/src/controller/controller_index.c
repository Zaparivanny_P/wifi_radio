#include "controller_index.h"

#include "../dispatcher.h"
#include "../webrender.h"

void controller_index(web_context *context)
{
    int res = web_fastrender(context, "demo/index.html");
    if(res != 0)
    {
        web_render_404(context);
    }
}

void controller_admin(web_context *context)
{
    /*char buffer[1024 * 100] = {0};
    char buffertempalte[1024  * 100] = {0};
    wb_template_var vars[] = {{.variable = "test", .value = "."},
                              {.variable = "name", .value = "user"}};

    int res = web_frender("admin\\index.html", buffertempalte, vars, 2);
    if(res == 0)
    {
        web_render_send(buffertempalte, buffer);
    }
    else
    {
        web_render_404();
    }*/

    int res = web_fastrender(context, "admin/index.html");
    if(res != 0)
    {
        web_render_404(context);
    }
}

void controller_simple(web_context *context)
{
    int res = web_fastrender(context, "index.html");
    if(res != 0)
    {
        web_render_404(context);
    }
}