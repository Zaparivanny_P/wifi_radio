#ifndef WEBSERVER_CONTROLLER_INDEX_H
#define WEBSERVER_CONTROLLER_INDEX_H

#include "webcontext.h"


void controller_index(web_context *context);
void controller_admin(web_context *context);
void controller_simple(web_context *context);


#endif //WEBSERVER_CONTROLLER_INDEX_H
