#ifndef WEBSERVER_WEBRENDER_H
#define WEBSERVER_WEBRENDER_H

#include "webcontext.h"

#define RENDER_BUFFER 512

typedef struct wb_template_var
{
    char *variable;
    char *value;
}wb_template_var;

int web_fastrender(web_context *context, const char *path);
int web_frender(const char *path, char *page, wb_template_var *v, int length);

int web_render(const char *template, char *page, wb_template_var *v, int length);
void web_render_send(const char *template, char *page);
void web_render_404(web_context *context);
int web_render_fsendb(web_context *context);


#endif //WEBSERVER_WEBRENDER_H
