#include "webserver.h"
#include "webcontext.h"
#include "dispatcher.h"

#include "string.h"
#include "stdio.h"
#include "stdlib.h"

#include "at_cmd.h"
#include "at_frame_manager.h"
#include "application.h"

static struct list_head request_q;
static uint8_t cnt_queue = 0;
extern uint8_t ipd_cnt_debug;
extern uint8_t ipd_id_buffer_debug[20];

void server_int()
{
    init_list_head(&request_q);
}

static enum WS_REQUEST get_type(char *data)
{
    int res = strcmp(data, "GET");
    if(res == 0)
    {
        return WS_GET;
    }
    
    res = strcmp(data, "POST");
    if(res == 0)
    {
        return WS_POST;
    }
    
    res = strcmp(data, "PUT");
    if(res == 0)
    {
        return WS_PUT;
    }
    
    res = strcmp(data, "DELETE");
    if(res == 0)
    {
        return WS_DELETE;
    }
    
    return WS_UNKNOW;
}

int server_read(int id, int length, char *message)
{
    char *nextptr = strpbrk(message, " ");
    char *nextptr2 = strpbrk(++nextptr, " ");
    int n = nextptr2 - nextptr;
    web_context* context = (web_context*)malloc(n + sizeof(web_context));
    
    if(context)
    {
        cnt_queue++;
        //memset(context, 0, n + sizeof(web_context));
        context->id = id;
        for(int i = 0; i < n - 1; i++)
        {
            char c = nextptr[i + 1];
            if(c == '?')
            {
                break;
            }
            context->url[i] = nextptr[i + 1];
        }
        context->url[n - 1] = 0;
        //printf("length path: %i, %s\n", n - 1, context->url);
        
        list_add_tail((struct list_head*)&context->list, &request_q);
        app_obj_signal_emit((void*)web_server_work);
        
    }
    else
    {
        printf("request ignore\n");
    }
    //dispatcher(context);
    return 1;
}

void web_server_work(void)
{    
    if(!list_empty(&request_q)) 
    {
        web_context *next = (web_context *)request_q.next;
        list_del(&next->list);
        //--cnt_queue;
        printf("del: 0x%X, cnt: %i, ipd: %i\n", next, cnt_queue, ipd_id_buffer_debug[ipd_cnt_debug]);
        dispatcher(next);
    }
}

void web_server_read(void)
{
    
    int id = at_manager_id();
    char* data = at_manager_buffer();
    server_read(id, 0, data);
}

void server_send(web_context *context, char* msg, uint32_t length)
{
    WiFi_reply* reply = malloc(sizeof(WiFi_reply));
    wifi_socket_client_write(reply, context->id, msg, length);
    while(reply->status == WIFI_BUSY);
    free(reply);
}

void server_socket_close(web_context *context)
{
    free(context);
    /*WiFi_reply* reply = malloc(sizeof(WiFi_reply));
    wifi_socket_client_close(reply, context->id);
    while(reply->status == WIFI_BUSY);
    free(reply);*/
}

void wifi_tcp_signal_connect(uint8_t id)
{
    //printf("%i\n", id);
}

void wifi_tcp_signal_closed(uint8_t id)
{
    
}