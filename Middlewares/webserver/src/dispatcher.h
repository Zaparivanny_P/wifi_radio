#ifndef WEBSERVER_DISPATCHER_H
#define WEBSERVER_DISPATCHER_H
#include "webcontext.h"

void dispatcher(web_context *context);

enum wb_extension
{
    EX_HTML,
    EX_JSON,
    EX_XML,
    EX_JPEG,
    EX_USER_DEFINED = 0x7f,
};

typedef struct wb_params
{
    int id;
    char *path;
    enum wb_extension ex;
}wb_params;

typedef struct wb_urlpattern
{
    char *pattern;
    void (*f)(web_context *context);
}wb_urlpattern;

#endif //WEBSERVER_DISPATCHER_H
