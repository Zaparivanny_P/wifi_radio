#ifndef WEBCONTEXT_H
#define WEBCONTEXT_H

#include "list.h"

enum WS_REQUEST
{
    WS_GET,
    WS_POST,
    WS_PUT,
    WS_DELETE,
    WS_UNKNOW,
};

typedef struct web_context
{
    struct list_head list;
    int id;
    enum WS_REQUEST type;
    char url[];
}web_context;

#endif //WEBCONTEXT_H