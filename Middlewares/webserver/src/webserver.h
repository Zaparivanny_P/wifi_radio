#ifndef WEBSERVER_WEBSERVER_H
#define WEBSERVER_WEBSERVER_H

#include "stdint.h"
#include "webcontext.h"

void server_int();
int server_read(int id, int length, char *message);
void web_server_read(void);
void web_server_work(void);


void server_start_send(web_context *context, char* msg, uint32_t length);
void server_socket_close(web_context *context);
#endif //WEBSERVER_WEBSERVER_H
