#include "dispatcher.h"
#include "stdio.h"
#include "webrender.h"

#include "re.h"

#include "controller/controllers.h"

void f_test(web_context *context);
void f_test2(web_context *context);

wb_urlpattern wb_urlpatterns[] = {{.pattern = "^articles/\\d+/$", .f = f_test},
                                  {.pattern = "^articles/\\d+/\\d+/$", .f = f_test2},
                                  {.pattern = "^demo/index$", .f = controller_index},
                                  {.pattern = "^admin/index$", .f = controller_admin},
                                  {.pattern = "^index$", .f = controller_simple}};


void f_test(web_context *context)
{
    char buffer[1024] = {0};
    char buffertempalte[1024] = {0};
    char template[1024] = "<html><body><a href=\"http://example.com/about.html#contacts\">Click here {{test}}</a></body></html>";


    wb_template_var vars[] = {{.variable = "test", .value = "."},
                              {.variable = "name", .value = "user"}};

    web_render(template, buffertempalte, vars, 2);
    web_render_send(buffertempalte, buffer);
}

void f_test2(web_context *context)
{
    char buffer[1024] = {0};
    char template[] = "<!DOCTYPE html>\n"
            "<html>\n"
            "<head>\n"
            "<!--script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script-->\n"
            "<script src=\"jquery.js\"></script>\n"
            "<script>\n"
            "$(document).ready(function(){\n"
            "    $(\"button\").click(function(){\n"
            "        $(\"p\").hide();\n"
            "    });\n"
            "});\n"
            "</script>\n"
            "</head>\n"
            "<body>\n"
            "\n"
            "<h2>This is a heading</h2>\n"
            "\n"
            "<p>This is a paragraph.</p>\n"
            "<p>This is another paragraph.</p>\n"
            "\n"
            "<button>Click me to hide paragraphs</button>\n"
            "\n"
            "</body>\n"
            "</html>";
    web_render_send(template, buffer);
}

void dispatcher(web_context *context)
{
    char *url = context->url;
    //printf("URL: %s\n", url);
    int i = 0;
    const int n = sizeof(wb_urlpatterns) / sizeof(wb_urlpatterns[0]);
    for(i = 0; i < n; i++)
    {
        re_t pattern = re_compile(wb_urlpatterns[i].pattern);
        int match_idx = re_matchp(pattern, url);
        if (match_idx != -1)
        {
            wb_urlpatterns[i].f(context);
            break;
        }
    }

    re_t pattern = re_compile("\\w+[.]\\w+$");
    int match_idx = re_matchp(pattern, url);
    if (match_idx != -1)
    {
        web_render_fsendb(context);
    }
    else if(n == i)
    {
        printf("No Match\n");
        web_render_404(context);
    }
}