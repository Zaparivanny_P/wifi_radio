#include "webrender.h"
#include "webserver.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"

#include "ff.h"
#include "stdint.h"
//"Connection: close\r\n"

static char *g_path = "0:/example/%s";
static char render_buffer[RENDER_BUFFER + 4];

static char *render_header =
        "HTTP/1.1 200 OK\r\n"
                "Server: IOT Web Server\r\n"
                "Content-Length: %lu\r\n\r\n";

int web_fastrender(web_context *context, const char *path)
{
    static FIL file;
    FRESULT resf;
    UINT cnt;
    memset(render_buffer, 0, RENDER_BUFFER);
    sprintf(render_buffer, g_path, path);
    printf("path file: %s\n", render_buffer);

    //resf = f_open(&file, "o:/example/demo/index.html", FA_READ);
    resf = f_open(&file, render_buffer, FA_READ);
    
    int res = -1;

    if (resf == FR_OK)
    {
        unsigned long lSize;

        lSize = file.fsize;
        
        printf("size file: %u\n", lSize);
        
        memset(render_buffer, 0, RENDER_BUFFER);
        sprintf(render_buffer, render_header, lSize);
        
        server_send(context, render_buffer, strlen(render_buffer));
        //server_write(render_buffer, strlen(render_buffer));


        memset(render_buffer, 0, RENDER_BUFFER);
        do {
            resf = f_read(&file, render_buffer, RENDER_BUFFER, &cnt);
            server_send(context, render_buffer, cnt);
            if(cnt != RENDER_BUFFER)
            {
                break;
            }
        }while(1);
        //for(int i = 0; i < 10000; i++) asm("nop");
        //server_send(context, "\r\n\r\n", 4);
        server_socket_close(context);
        //server_write("\r\n\r\n", 4);

        f_close(&file);
        res = 0;
    }
    else
    {
        printf("error code open file: %i\n", resf);
    }
    return res;
}

int web_fastrender2(const char *path)
{
    /*FILE *pFile;

    memset(render_buffer, 0, RENDER_BUFFER);
    sprintf(render_buffer, g_path, path);
    printf("path file: %s\n", render_buffer);

    pFile = fopen(render_buffer, "r");
    int res = -1;

    if (pFile != NULL)
    {
        long lSize;

        fseek(pFile, 0, SEEK_END);
        lSize = ftell(pFile);
        rewind(pFile);

        memset(render_buffer, 0, RENDER_BUFFER);
        sprintf(render_buffer, render_header, lSize);
        server_send(render_buffer, 0);


        memset(render_buffer, 0, RENDER_BUFFER);
        do {
            int result = fread(render_buffer, 1, RENDER_BUFFER, pFile);
            server_sendb(render_buffer, result, 0);
            if(result != RENDER_BUFFER)
            {
                break;
            }
        }while(1);
        server_sendb("\r\n\r\n", 4, 1);

        fclose(pFile);
    }
    return res;*/
}

int web_frender(const char *path, char *page, wb_template_var *v, int length)
{
    /*FILE *pFile;

    memset(render_buffer, 0, RENDER_BUFFER);
    sprintf(render_buffer, g_path, path);
    printf("path file: %s\n", render_buffer);

    pFile = fopen(render_buffer, "r");
    int res = -1;

    if (pFile != NULL)
    {
        long lSize;

        fseek(pFile, 0, SEEK_END);
        lSize = ftell(pFile);
        rewind(pFile);

        char *fbuffer2 = (char*)malloc(sizeof(char) * lSize);
        if(fbuffer2 == NULL)
        {
            web_render_404();
        }
        else
        {
            memset(fbuffer2, 0, sizeof(char) * lSize);
            int result = fread(fbuffer2, 1, lSize, pFile);
            if (result > 0)
            {
                res = web_render(fbuffer2, page, v, length);
            }
            free(fbuffer2);
        }

        fclose(pFile);
    }
    return res;*/
}

int web_render(const char *template, char *page, wb_template_var *v, int length) // TODO NO TESTED FUNCTION
{
    int state = 0;
    int iterator_page = 0;
    for(int i = 0; i < strlen(template); i++)
    {
        char c = template[i];

        if(c == '{' && template[i + 1] == '{')
        {
            i += 2;
            if(i >= strlen(template))
            {
                break;
            }
            state = 1;
        }

        switch (state)
        {
            case 0:
                page[iterator_page++] = c;
                break;
            case 1:
                for(int j = 0; j < length; j++)
                {
                    int res = strncmp(template + i, v[j].variable, strlen(v[j].variable));
                    if(res == 0)
                    {
                        for(int k = 0; k < strlen(v[j].value); k++)
                        {
                            page[iterator_page++] = v[j].value[k];
                        }
                        state = 2;
                    }
                }
                break;
            case 2:
                if(c == '}' && template[i + 1] == '}')
                {
                    i++;
                    state = 0;
                }
                break;
        }
    }
    return 0;
}

void web_render_send(const char *template, char *page)
{
    char *message2 =
            "HTTP/1.1 200 OK\r\n"
            "Date: Wed, 11 Feb 2009 11:20:59 GMT\r\n"
            "Server: IOT Web Server\r\n"
            "Content-Language: ru\r\n"
            "Content-Type: text/html; charset=utf-8\r\n"
            "Content-Length: %i\r\n\r\n"
            "%s\r\n\r\n";

    sprintf(page, message2, strlen(template), template);

    //printf("server render: %s", page);
    //server_send(page, 1);
    
}

void web_render_404(web_context *context)
{
    char *message2 =
            "HTTP/1.1 404 Not Found\r\n"
            "Date: Wed, 11 Feb 2009 11:20:59 GMT\r\n"
            "Server: IOT Web Server\r\n"
            "Content-Language: ru\r\n"
            "Content-Type: text/html; charset=utf-8\r\n"
            "Content-Length: 197\r\n\r\n"
            "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">"
            "<html><head><title>404 Not Found</title></head><body><h1>Not Found</h1>"
            "<p>The requested URL was not found on this server.</p></body></html>\r\n\r\n";

    //server_send(message2, 1);
    server_send(context, message2, strlen(message2));
}

int web_render_fsendb2(const char *path)
{
    /*FILE *pFile;

    memset(render_buffer, 0, RENDER_BUFFER);
    sprintf(render_buffer, g_path, path);
    printf("path file: %s\n", render_buffer);

    pFile = fopen(render_buffer, "rb");
    int res = -1;

    if (pFile != NULL)
    {
        long lSize;

        fseek(pFile, 0, SEEK_END);
        lSize = ftell(pFile);
        rewind(pFile);

        memset(render_buffer, 0, RENDER_BUFFER);
        sprintf(render_buffer, render_header, lSize);
        server_send(render_buffer, 0);

        memset(render_buffer, 0, RENDER_BUFFER);
        do {
            int result = fread(render_buffer, 1, RENDER_BUFFER, pFile);
            server_sendb(render_buffer, result, 0);
            if(result != RENDER_BUFFER)
            {
                break;
            }
        }while(1);
        server_sendb("\r\n\r\n", 4, 1);

        fclose(pFile);
    }
    return res;*/
}

int web_render_fsendb(web_context *context)
{
    return web_fastrender(context, context->url);
}