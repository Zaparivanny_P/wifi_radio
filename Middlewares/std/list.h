#ifndef LIST_H
#define LIST_H

#include "string.h"

#define WRITE_ONCE(type, var, val) \
	 (*((volatile type *)(&(var))) = (val))

#define READ_ONCE(type, var) (*((volatile type *)(&(var))))

struct list_head 
{
	struct list_head *prev;
	struct list_head *next;
};

static inline void init_list_head(struct list_head *list)
{
	//WRITE_ONCE(list->next, list);
    WRITE_ONCE(struct list_head*, list->next, list);
	list->prev = list;
}


static inline void __list_add(struct list_head *list_new,
							  struct list_head *prev,
							  struct list_head *next)
{
	next->prev = list_new;
	list_new->next = next;
	list_new->prev = prev;
    prev->next = list_new;
}

//Insert a new entry after the specified head. This is good for implementing stacks. 
static inline void list_add(struct list_head *list_new, struct list_head *head)
{
	__list_add(list_new, head, head->next);
}

//Insert a new entry before the specified head. This is useful for implementing queues.
static inline void list_add_tail(struct list_head *list_new, struct list_head *head)
{
	__list_add(list_new, head->prev, head);
}

static inline void __list_del(struct list_head * prev, struct list_head * next)
{
	next->prev = prev;
    prev->next = next;
}

static inline void __list_del_entry(struct list_head *entry)
{
    if (!__list_del_entry_valid(entry))
        return;
	__list_del(entry->prev, entry->next);
}

static inline void list_del(struct list_head *entry)
{
	__list_del(entry->prev, entry->next);
	entry->next = NULL;
	entry->prev = NULL;
}

static inline void list_replace(struct list_head *old,
				struct list_head *list_new)
{
	list_new->next = old->next;
	list_new->next->prev = list_new;
	list_new->prev = old->prev;
	list_new->prev->next = list_new;
}

static inline int list_empty(const struct list_head *head)
{
	return head->next == head;
}

/**************************hlist_head*******************/
struct hlist_head {
	struct hlist_node *first;
};

struct hlist_node {
	struct hlist_node *next, **pprev;
};

#define HLIST_HEAD_INIT { .first = NULL }
#define HLIST_HEAD(name) struct hlist_head name = {  .first = NULL }
#define INIT_HLIST_HEAD(ptr) ((ptr)->first = NULL)

static inline void hlist_init(struct hlist_node *h)
{
	h->next = NULL;
	h->pprev = NULL;
}

static inline int hlist_unhashed(const struct hlist_node *h)
{
	return !h->pprev;
}

static inline int hlist_empty(const struct hlist_head *h)
{
	return !READ_ONCE(struct hlist_head*, h->first);
}

static inline void __hlist_del(struct hlist_node *n)
{
	struct hlist_node *next = n->next;
	struct hlist_node **pprev = n->pprev;

	WRITE_ONCE(struct hlist_node*, *pprev, next);
	if (next)
		next->pprev = pprev;
}

static inline void hlist_del(struct hlist_node *n)
{
	__hlist_del(n);
	n->next = NULL;
	n->pprev = NULL;
}

static inline void hlist_add_head(struct hlist_node *n, struct hlist_head *h)
{
	struct hlist_node *first = h->first;
	n->next = first;
	if (first)
		first->pprev = &n->next;
	WRITE_ONCE(struct hlist_node*, h->first, n);
	n->pprev = &h->first;
}

static inline void hlist_add_before(struct hlist_node *n,
					struct hlist_node *next)
{
	n->pprev = next->pprev;
	n->next = next;
	next->pprev = &n->next;
	WRITE_ONCE(struct hlist_node*, *(n->pprev), n);
}

static inline void hlist_add_behind(struct hlist_node *n,
				    struct hlist_node *prev)
{
	n->next = prev->next;
	WRITE_ONCE(struct hlist_node*, prev->next, n);
	n->pprev = &prev->next;

	if (n->next)
		n->next->pprev  = &n->next;
}
#define typeof(a) (type)(a)

//#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)

#define container_of(ptr, type, member) ({                      \
        const typeof( ((type *)0)->member ) *__mptr = (ptr);    \
        (type *)( (char *)__mptr - offsetof(type,member) );})

#define hlist_entry(ptr, type, member) container_of(ptr,type,member)

#define hlist_for_each(pos, head) \
	for (pos = (head)->first; pos ; pos = pos->next)

#define hlist_entry_safe(ptr, type, member) \
	({ typeof(ptr) ____ptr = (ptr); \
	   ____ptr ? hlist_entry(____ptr, type, member) : NULL; \
	})

#define hlist_for_each_entry(pos, head, member)				\
	for (pos = hlist_entry_safe((head)->first, typeof(*(pos)), member);\
	     pos;							\
	     pos = hlist_entry_safe((pos)->member.next, typeof(*(pos)), member))

#endif