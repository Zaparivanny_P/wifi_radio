#ifndef BUTTON_H
#define BUTTON_H

#include "stm32f10x.h"
#include "board.h"

#define BTN_TIME_LIMIT 60

#define __GET_PORT(a) GPIO ## a
#define BTN_PORT(a) __GET_PORT(a)

#define __GET_PIN(a) GPIO_PIN_ ## a
#define BTN_PIN(a) __GET_PIN(a)

typedef enum
{
	BTN_READY 		= 0x0,
	BTN_SHORT_PRESS = 0x1,
	BTN_LONG_PRESS  = 0x2,
	BTN_PRESSED,
	BTN_LONG_PRESSED,
}Btn_state;

typedef struct Button
{
	GPIO_TypeDef* GPIOx;
	uint16_t GPIO_Pin;
	Btn_state status;
	uint32_t time;
}Button;

typedef struct Buttons
{
	Button *b;
	uint8_t pcs;
}Buttons;

void btn_init(Buttons* btn);
Buttons* btn_update();
void btn_reset();
void btn_tick();


uint8_t btn_is_pressed(Button* btn);

#endif
