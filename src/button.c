#include "button.h"
#include "stddef.h"

Buttons *_btn;
uint32_t btn_time;

void btn_init(Buttons* btn)
{
	_btn  = btn;
}

Buttons* btn_update(Buttons* btn)
{
	for(uint8_t i = 0; i <  _btn->pcs; i++)
	{
		if((_btn->b[i].status == BTN_READY) && btn_is_pressed(&_btn->b[i]))
		{
			_btn->b[i].time = btn_time;
			_btn->b[i].status = BTN_PRESSED;
		}
		else if((_btn->b[i].status == BTN_PRESSED) && !btn_is_pressed(&_btn->b[i]))
		{
			_btn->b[i].status = BTN_SHORT_PRESS;
		}
		else if(((btn_time - _btn->b[i].time) > BTN_TIME_LIMIT) && 
                (_btn->b[i].status == BTN_PRESSED))
		{
			_btn->b[i].status = BTN_LONG_PRESS;
		}
	}
	return _btn;
}

void btn_reset()
{
	for(uint8_t i = 0; i < _btn->pcs; i++)
	{
		if(btn_is_pressed(&_btn->b[i]) && _btn->b[i].status == BTN_LONG_PRESS)
		{
			_btn->b[i].status = BTN_LONG_PRESSED;
		}
        if((_btn->b[i].status != BTN_PRESSED) && (!btn_is_pressed(&_btn->b[i])))
            _btn->b[i].status = BTN_READY;
	}
}

void btn_tick()
{
	if(++btn_time == 0)
	{
		if(_btn == NULL)
			return;
		for(uint8_t i = 0; i < _btn->pcs; i++)
		{
			_btn->b[i].time = 0;
		}
	}
}

uint8_t btn_is_pressed(Button* btn)
{
    static uint16_t _res ;
    _res = (~GPIO_ReadInputData(btn->GPIOx) >> btn->GPIO_Pin);
	return  (_res) & 0x1;
} 