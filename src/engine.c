#include "engine.h"
#include "sdcard.h"

#include "board.h"
#include "vs1003.h"
#include "stdlib.h"

#include "button.h"
#include "directory.h"
#include "string.h"

#include "view2x1.h"

#include "playlist.h"
#include "playlist_m3u.h"
#include "playlist_v1.h"

#include "audio_stream.h"
#include "at_frame_manager.h"
#include "application.h"
#include "webserver.h"

FIL audio_file;
uint8_t engine_buffer[512];

void engine_sm_control();
void (*state_mashine_controller)() = engine_sm_control;

static Playlist *engine_playlist;

Dir engine_dir;

uint8_t _engine_pause = 1;

uint8_t engine_volume = 0x30;

void engine_init()
{
    audio_stream_init();
	at_frame_register_callback(1, audio_stream_write_frame, audio_stream_create_frame);
    app_connect(at_frame_manager_complite, web_server_read, CONNECT_BLOCKED);
    app_connect(web_server_work, web_server_work, CONNECT_DIRECT);
    server_int();
	
	static Buttons btns;
	static Button btn[3];
	btns.b = btn;
	btns.pcs = sizeof(btn) / sizeof(btn[0]);
	
	btn[0].GPIOx = _PORT(PORT_BTN_1);
	btn[0].GPIO_Pin = PIN_BTN_1;
	btn[1].GPIOx = _PORT(PORT_BTN_2;
	btn[1].GPIO_Pin = PIN_BTN_2);
	btn[2].GPIOx = _PORT(PORT_BTN_3);
	btn[2].GPIO_Pin = PIN_BTN_3;
	
	btn_init(&btns);
	
	dir_path(&engine_dir, "0:/");
   	
	Class_playlist* class_tree = playlist_singletion_factory(PE_TREE);
	engine_playlist = class_tree->create(class_tree);
    engine_playlist->this->load(engine_playlist, "0:/");
	
    //engine_low_power();
	uint8_t *path;
	engine_playlist->this->next(engine_playlist, &path);
	f_open(&audio_file, (char const*)path, FA_READ);
    vs1003_send(0);
	vs1003_send(0);
	vs1003_send(0);
	vs1003_send(0);
}

void engine_low_power()
{
    RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);
	RCC_PLLCmd(DISABLE);
	RCC_PLLConfig(RCC_PLLSource_HSI_Div2, RCC_PLLMul_4);
    RCC_PLLCmd(ENABLE);

    while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET) {}
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
}

void engine_high_power()
{
    RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);
    
	RCC_PLLCmd(DISABLE);
	RCC_PLLConfig(RCC_PLLSource_HSI_Div2, RCC_PLLMul_12);
    RCC_PLLCmd(ENABLE);

    while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET) {}
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
    
    engine_reinit_to_high_power();
}

void engine_sm_to_low_power()
{
    if(view_processed())
        return;
    
	//engine_low_power();
    engine_reinit_to_low_power();
    
	state_mashine_controller = engine_sm_control;
}

void engine_reinit_to_high_power()
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    TIM_TimeBaseInitStruct.TIM_Prescaler = 4 * 12 * 100;
    TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Down;
    TIM_TimeBaseInitStruct.TIM_Period = 100;
    TIM_TimeBaseInitStruct.TIM_ClockDivision = 0;
    TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM5, &TIM_TimeBaseInitStruct);
}

void engine_reinit_to_low_power()
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    TIM_TimeBaseInitStruct.TIM_Prescaler = 4 * 4 * 100;
    TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Down;
    TIM_TimeBaseInitStruct.TIM_Period = 100;
    TIM_TimeBaseInitStruct.TIM_ClockDivision = 0;
    TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM5, &TIM_TimeBaseInitStruct);
}

void engine_sm_volume_control()
{
	Buttons* btn = btn_update();
	
    if(btn->b[BTN_NEXT].status == BTN_SHORT_PRESS)
    {
        if(engine_volume > 5)
        {
            while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);
            engine_volume -= 5;
            view_volume_control(engine_volume);
            vs1003_write_reg(SPI_VOL, engine_volume, engine_volume);
            while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);
        }
    }

	if(btn->b[BTN_PREW].status == BTN_SHORT_PRESS)
	{
        if(engine_volume < 250)
        {
            while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);
            engine_volume += 5;
            view_volume_control(engine_volume);
            vs1003_write_reg(SPI_VOL, engine_volume, engine_volume);
            while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);
        }
	}
	
	if(btn->b[BTN_PREW].status == BTN_LONG_PRESS)
	{
        view_off();
		state_mashine_controller = engine_sm_to_low_power;
	}
	
	if(btn->b[BTN_NEXT].status == BTN_LONG_PRESS)
	{
        view_off();
		state_mashine_controller = engine_sm_to_low_power;
	}
	
    btn_reset();
}

void engine_sm_select_playlist()
{
	Buttons* btn = btn_update();
	static uint8_t playlist_type = 0;
	
	if(engine_playlist != NULL && playlist_type == 0)
	{
		playlist_type = engine_playlist->type;
		veiw_disp_playlist(playlist_type);
	}
    else
    {
        veiw_disp_playlist(playlist_type);
    }
	
	
    if(btn->b[BTN_NEXT].status == BTN_SHORT_PRESS)
    {
		if(playlist_type < PLAYLISTS)
			playlist_type++;
		veiw_disp_playlist(playlist_type);
    }

	if(btn->b[BTN_PREW].status == BTN_SHORT_PRESS)
	{
		if(playlist_type > 1)
			playlist_type--;
		veiw_disp_playlist(playlist_type);
	}
	
	if(btn->b[BTN_ENTR].status == BTN_SHORT_PRESS)
	{

	}
	
	if(btn->b[BTN_PREW].status == BTN_LONG_PRESS)
	{
        view_off();
		state_mashine_controller = engine_sm_to_low_power;
	}
	
	if(btn->b[BTN_NEXT].status == BTN_LONG_PRESS)
	{
        view_off();
		state_mashine_controller = engine_sm_to_low_power;
	}
	
    btn_reset();
}

void engine_sm_select_path()
{
	Buttons* btn = btn_update();
	
	
    if(btn->b[BTN_NEXT].status == BTN_SHORT_PRESS)
    {
		dir_next(&engine_dir);
        view_disp_path(dir_current_dir(&engine_dir));
    }

	if(btn->b[BTN_PREW].status == BTN_SHORT_PRESS)
	{
		dir_prev(&engine_dir);
        view_disp_path(dir_current_dir(&engine_dir));
	}
	
	if(btn->b[BTN_ENTR].status == BTN_SHORT_PRESS)
	{
		dir_enter(&engine_dir);
	}
	
	if(btn->b[BTN_ENTR].status == BTN_LONG_PRESS)
	{
		dir_out(&engine_dir);
	}
	
	if(btn->b[BTN_PREW].status == BTN_LONG_PRESS)
	{
		view_off();
		state_mashine_controller = engine_sm_to_low_power;
	}
	
	if(btn->b[BTN_NEXT].status == BTN_LONG_PRESS)
	{
		view_off();
        engine_pause();
		uint8_t* p = dir_current_path(&engine_dir);
		
		
		engine_playlist->this->load(engine_playlist, p);
        engine_next_audio_file();
        engine_resume();
		
		state_mashine_controller = engine_sm_to_low_power;
	}
	
    btn_reset();
}

void engine_sm_control()
{
	Buttons* btn = btn_update();
    if(btn->b[BTN_NEXT].status == BTN_SHORT_PRESS)
    {
        engine_next_audio_file();
    }

	if(btn->b[BTN_PREW].status == BTN_SHORT_PRESS)
	{
		engine_prev_audio_file();
	}
        
    if(btn->b[BTN_ENTR].status == BTN_SHORT_PRESS)
	{
       _engine_pause ^= 1;
	}
    
	if(btn->b[BTN_ENTR].status == BTN_LONG_PRESS)
	{
		engine_high_power();
        view_disp_path(dir_current_dir(&engine_dir));
		state_mashine_controller = engine_sm_select_path;
	}
	
	if(btn->b[BTN_NEXT].status == BTN_LONG_PRESS)
	{
		engine_high_power();
		state_mashine_controller = engine_sm_select_playlist;
	}
	
	if(btn->b[BTN_PREW].status == BTN_LONG_PRESS)
	{
		engine_high_power();
		view_volume_control(engine_volume);
		state_mashine_controller = engine_sm_volume_control;
	}
	
	
    btn_reset();
}

void engine_update()
{
	/*if(_engine_pause)
		engine_stream_audio();
	state_mashine_controller();*/
    static uint32_t cnt_frame = 0;
	if(audio_stream_frame_is_complite() && audio_stream_is_ready())
	{
        cnt_frame++;
		static uint32_t len;
        len = audio_stream_length();
        
        audio_stream_read();
        audio_stream_read();
        audio_stream_read();
        audio_stream_read();
        
		VS1003_XDCS_HIGH();
		for(uint32_t i = 0; i < len; i++)
		{
			while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);
			vs1003_send(audio_stream_read());
		}
		VS1003_XDCS_LOW();
	}
    
}

void engine_stream_audio()
{
	UINT nRead;
	if(f_read(&audio_file, engine_buffer, sizeof(engine_buffer), &nRead) == 0)
	{
		if(nRead)
		{
			VS1003_XDCS_HIGH();
			for (uint16_t i = 0; i < nRead; i++)
			{
				while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);
				vs1003_send(engine_buffer[i]);
			}
			VS1003_XDCS_LOW();
		}
		else
		{
			engine_next_audio_file();
		}
	}
}


void engine_next_audio_file()
{
	f_close(&audio_file);
	
	uint8_t *_audio_file;
    if(engine_playlist->this->next(engine_playlist, &_audio_file) != PL_OK)
    {
        engine_pause();
        return;
    }
    
	f_open(&audio_file, (char const*)_audio_file, FA_READ);
	
	
	vs1003_send(0);
	vs1003_send(0);
	vs1003_send(0);
	vs1003_send(0);
}

void engine_prev_audio_file()
{
    
	f_close(&audio_file);
    
	uint8_t *_audio_file;
	if(engine_playlist->this->prev(engine_playlist, &_audio_file) != PL_OK)
    {
        engine_pause();
        return;
    }
    
	f_open(&audio_file, (char const*)_audio_file, FA_READ);
	
	vs1003_send(0);
	vs1003_send(0);
	vs1003_send(0);
	vs1003_send(0);
}

void engine_pause()
{
	_engine_pause = 0x0;
}

void engine_resume()
{
	_engine_pause = 0x1;
}