#include "stm32f10x.h"
#include "sdcard.h"
#include "ws2812.h"
#include "board.h"
#include "button.h"
#include "engine.h"
#include "vs1003.h"
#include "stdlib.h"
#include "view2x1.h"
#include "setting.h"
#include "application.h"
#include "wifi_example.h"


#ifdef UNIT_TEST
#include "test_allocation.h"
#endif

FATFS FATFS_Obj;

uint32_t global_time;

void init_tim5()
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    NVIC_InitTypeDef NVIC_InitStruct;


    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);

    NVIC_InitStruct.NVIC_IRQChannel = TIM5_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStruct);
    NVIC_SetPriority(TIM5_IRQn, NVIC_EncodePriority(4, 2, 0));
            
    TIM_TimeBaseInitStruct.TIM_Prescaler = 4 * 4 * 100;
    TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Down;
    TIM_TimeBaseInitStruct.TIM_Period = 100;
    TIM_TimeBaseInitStruct.TIM_ClockDivision = 0;
    TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM5, &TIM_TimeBaseInitStruct);
    TIM_ITConfig(TIM5, TIM_IT_Update, ENABLE);
    TIM_Cmd(TIM5, ENABLE);
    
    GPIO_InitTypeDef GPIO_InitStruct;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
    
    
    GPIO_Init(GPIOA, &GPIO_InitStruct);
}

void TIM5_IRQHandler()
{
    TIM_ClearFlag(TIM5, TIM_FLAG_Update);
    global_time++;
    btn_tick();
    view_tick();
}


void playlist_load()
{
    static FIL fp;
    static UINT bw, nRead;
    static FRESULT res;
    uint8_t _buffer[100];
    uint8_t *ptr;
    //res = f_open(&fp, "0:/config.txt", FA_WRITE | FA_CREATE_ALWAYS);    
    //res = f_write(&fp, "test2245", sizeof("test22"), &bw);  
    //f_close(&fp);
    Setting stg;
    setting_create(&stg);
    
    res = f_open(&fp, "0:/config.txt", FA_READ);
    //f_read(&fp, _buffer, sizeof(_buffer), &nRead);
    do
    {
        ptr = f_gets(_buffer, sizeof(_buffer), &fp);
        setting_parse(&stg, ptr);
    }while(ptr != NULL);
    f_close(&fp);
    
    setting_free(&stg);
}

void filetest()
{
    static FIL filetest;
    static UINT bw, nRead;
    static FRESULT res;
    uint8_t _buffer[100];
    uint8_t *ptr;
    
    res = f_open(&filetest, "0:/example/demo/index.html", FA_READ);
    //f_read(&fp, _buffer, sizeof(_buffer), &nRead);
    do
    {
        ptr = f_gets(_buffer, sizeof(_buffer), &filetest);
    }while(ptr != NULL);
    f_close(&filetest);
}

void main()
{
    static FRESULT result;
    /*GPIO_InitTypeDef GPIO_InitStruct;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOA, &GPIO_InitStruct);*/
   // GPIO_SetBits(GPIOA, GPIO_Pin_2);
    //init_tim5();
    
    FLASH_SetLatency(FLASH_Latency_2);

    RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_6);
    //RCC_PLLConfig(RCC_PLLSource_HSI_Div2, RCC_PLLMul_12);
    RCC_PLLCmd(ENABLE);

    while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET) {}
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
    
   
    
    //view_disp_path(NULL);
    
    //WS2812_send(0x00000001);
   // WS2812_send_coloru8(101 / 5, 0, 106 / 5);
    //WS2812_send_coloru8(0x5, 0x5, 0x1);
    
    //WS2812_send_RGB(255, 0, 0);
    //WS2812_send_RGB(0, 255, 0);
    //WS2812_send_RGB(0, 0, 255);
    /*uint8_t l = 100;
    uint8_t s = 50;
    
    WS2812_send_HSV(0, s, l);
    WS2812_send_HSV(10, s, l);
    WS2812_send_HSV(20, s, l);
    WS2812_send_HSV(30, s, l);
    WS2812_send_HSV(40, s, l);
    WS2812_send_HSV(50, s, l);
    WS2812_send_HSV(60, s, l);*/
    
    /*for(uint16_t i = 0; i < 360; i++)
    {
        WS2812_send_HSV(i, 50, 100);
        for(uint32_t j = 0; j <= 100; j++)
        {
            WS2812_send_HSV(i, 100, j);
            for(uint32_t k = 0; k < 100000; k++);
        }
    }*/
        
    sdcard_init();
    
    result = f_mount(&FATFS_Obj, "0", 1);
    if(result != FR_OK)
    {
        while(1);
    }

    //filetest();
    _delay(10000);
    board_init();
    ///init_tim5();
    //vs1003_start();
    ///playlist_load();
    //vs1003_sine_test(); 
    vs1003_start();
    //s1003_set_volume(0xA0);
    //vs1003_set_volume(0x01);
    //02-Sad But True.mp3
    //"0:/explosive/boom.wav"
    //tmp = vs1003_get_decode_time();
#ifdef UNIT_TEST
	test_allocation();
#endif
    
    //esp_versions();
    engine_init();
    wifi_example();
    //
	//GPIO_SetBits(_PORT(PORT_TFT_NE1), _PIN(PIN_TFT_NE1));
    while(1)
    {
        engine_update();
        app_exec();
    }
}