#ifndef WEBROUTERS_H
#define WEBROUTERS_H

#include "webserver.h"
#include <stdarg.h>

/*
#define WR_GET_INDEX(a) {.type = WS_GET,    .f_index = a##_index}
#define WR_GET_NEW(a)   {.type = WS_GET,    .f_new = a##_new}
#define WR_GET_ID(a)    {.type = WS_GET,    .f_show = a##_show}
#define WR_GET_EDIT(a)  {.type = WS_GET,    .f_edit = a##_edit}
#define WR_POST(a)      {.type = WS_POST,   .f_create = a##_create}
#define WR_PUT(a)       {.type = WS_PUT,    .f_update = a##_update}
#define WR_DELETE(a)    {.type = WS_DELETE, .f_destroy = a##_destroy}



typedef struct web_params
{
    int id;
}web_params;


typedef struct webroute
{
    char *prefix;
    char *methodname;
    void (*f_index)(web_params *params);
    void (*f_new)(web_params *params);
    void (*f_show)(web_params *params);
    void (*f_edit)(web_params *params);
    void (*f_create)(web_params *params);
    void (*f_update)(web_params *params);
    void (*f_destroy)(web_params *params);
}webroute;*/


typedef struct urlpattern
{
    char *pattern;
    //void (*f)(...);
}urlpattern;

void web_route(char *path);

#endif

