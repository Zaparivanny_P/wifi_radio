#include "webserver.h"
#include "at_frame_manager.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "at_cmd.h"
#include "application.h"
#include "webrouters.h"



//example
//GET /favicon.ico HTTP/1.1
//Host: 192.168.5.1
//User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0
//Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
//Accept-Language: en-US,en;q=0.5
//Accept-Encoding: gzip, deflate
//Connection: keep-alive



void web_tmp(void *obj)
{
    app_obj_disconnect_signal(obj);
    
    
    
    free(obj);
}


static enum WS_REQUEST get_type(char *data)
{
    int res = strcmp(data, "GET");
    if(res == 0)
    {
        return WS_GET;
    }
    
    res = strcmp(data, "POST");
    if(res == 0)
    {
        return WS_POST;
    }
    
    res = strcmp(data, "PUT");
    if(res == 0)
    {
        return WS_PUT;
    }
    
    res = strcmp(data, "DELETE");
    if(res == 0)
    {
        return WS_DELETE;
    }
    
    return WS_UNKNOW;
}

void web_server_read(void)
{
    uint16_t id = at_manager_id();
    char* data = at_manager_buffer();
    printf("%i, %s\n", id, data);
    
    WiFi_reply *reply = (WiFi_reply *)malloc(sizeof(WiFi_reply));
    app_obj_connect(reply, web_tmp, CONNECT_DIRECT);
    
    
    //get_type(data);
    
    char *nextptr = strpbrk(data, " ");
    nextptr++;
    
    web_route(nextptr);
    
    static char buffer[] = 
        "HTTP/1.1 200 OK \r\n"
        "Content-Type: text/html; charset=utf-8\r\n"
        "Content-Length: 89 \r\n\r\n"
        "<html><body><a href=\"http://example.com/about.html#contacts\">Click here</a></body></html> \r\n\r\n";
    wifi_socket_client_write(reply, id, buffer, strlen(buffer));
}