#ifndef APPLICATION_H
#define APPLICATION_H

#include "stdint.h"

#define MAX_CONNECTIONS 20

typedef void (*FSignal)(void);
typedef void (*FSlot)(void);

typedef void (*FObjSlot)(void* obj);

typedef enum con_type
{
	CONNECT_DIRECT = 1,
	CONNECT_BLOCKED = 2,
}con_type;

#define SIGNATURE_FNC 0x80
#define SIGNATURE_OBJ 0x40


void app_exec(void);


void app_void_connect(void *obj, void* slot, con_type type, uint8_t sig);
void app_void_disconnect(void *obj, void* slot);
void app_void_disconnect_signal(void *obj);

void app_obj_connect(void *obj, FObjSlot slot, con_type type);
void app_obj_disconnect(void *obj, FObjSlot slot);
void app_obj_disconnect_signal(void *obj);
void app_obj_signal_emit(void *obj);

void app_connect(FSignal signal, FSignal slot, con_type type);
void app_disconnect(FSignal signal, FSignal slot);
void app_disconnect_signal(FSignal signal);


#endif