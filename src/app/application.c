#include "application.h"
#include "string.h"

typedef struct conenction_node
{
	void* signal;
	void* slot;
	con_type type;
	uint8_t is_active;
}conenction_node;

void app_signal_emit(conenction_node *node);

conenction_node connection_list[MAX_CONNECTIONS];

void app_void_connect(void *obj, void* slot, con_type type, uint8_t mode)
{
	for(uint16_t i = 0; i < MAX_CONNECTIONS; i++)
	{
		if(connection_list[i].signal == NULL)
		{
			connection_list[i].signal = obj;
			connection_list[i].slot = slot;
			connection_list[i].type = (uint8_t)type | mode;
			connection_list[i].is_active = 0;
			break;
		}
	}
}

void app_void_disconnect(void *obj, void* slot)
{
	for(uint16_t i = 0; i < MAX_CONNECTIONS; i++)
	{
		if(connection_list[i].signal == obj && connection_list[i].slot == slot)
		{
			connection_list[i].signal = NULL;
		}
	}
}

void app_void_disconnect_signal(void *obj)
{
	for(uint16_t i = 0; i < MAX_CONNECTIONS; i++)
	{
		if(connection_list[i].signal == obj)
		{
			connection_list[i].signal = NULL;
		}
	}
}

void app_exec(void)
{
	for(uint16_t i = 0; i < MAX_CONNECTIONS; i++)
	{
		if(connection_list[i].is_active)
		{
            connection_list[i].is_active = 0;
			app_signal_emit(&connection_list[i]);
		}
	}
}

void app_obj_signal_emit(void *obj)
{
	for(uint16_t i = 0; i < MAX_CONNECTIONS; i++)
	{
		if(connection_list[i].signal == obj)
		{
			if(connection_list[i].type == CONNECT_BLOCKED)
			{
				app_signal_emit(&connection_list[i]);
			}
			else
			{
				connection_list[i].is_active = 1;
			}
		}
	}
}

void app_signal_emit(conenction_node *node)
{
	if(node->type & SIGNATURE_FNC)
	{
		FSlot f;
		f = (FSlot)node->slot;
		f();
	}
	else
	{
		FObjSlot f;
		f = (FObjSlot)node->slot;
		f(node->signal);
	}
}

void app_obj_connect(void *obj, FObjSlot slot, con_type type)
{
	app_void_connect(obj, (void*)slot, type, SIGNATURE_OBJ);
}

void app_obj_disconnect(void *obj, FObjSlot slot)
{
	app_void_disconnect(obj, (void*)slot);
}

void app_obj_disconnect_signal(void *obj)
{
	app_void_disconnect_signal(obj);
}

void app_connect(FSignal signal, FSignal slot, con_type type)
{
	app_void_connect((void*)signal, (void*)slot, type, SIGNATURE_FNC);
}

void app_disconnect(FSignal signal, FSignal slot)
{
	app_void_disconnect((void*)signal, (void*)slot);
}

void app_disconnect_signal(FSignal signal)
{
	app_void_disconnect_signal((void*)signal);
}

