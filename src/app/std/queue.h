#ifndef QUEUE_H
#define QUEUE_H

#include "stdint.h"


#define QUEUE_SIZE 20

typedef struct Queue
{
	void *queue[QUEUE_SIZE];
	uint16_t index_back;
	uint16_t index_front;
}Queue;

int8_t queue_push_back(Queue *q, void *val);
void *queue_pop_front(Queue *q);
int8_t queue_is_empty(Queue *q);

#endif