#include "queue.h"
#include "stddef.h"

uint16_t queue_next_index(uint16_t index);

uint16_t queue_next_index(uint16_t index)
{
	index++;
	return index == QUEUE_SIZE ? 0 : index;
}

int8_t queue_push_back(Queue *q, void *val)
{
	uint16_t next_index = queue_next_index(q->index_back);
	if(next_index == q->index_front)
	{
		return -1;
	}
	
	q->queue[q->index_back] = val;
	q->index_back = next_index;
	return 0;
}

void *queue_pop_front(Queue *q)
{
	void *val = NULL;
	if(q->index_back != q->index_front)
	{
		val = q->queue[q->index_front];
		q->index_front = queue_next_index(q->index_front);
	}
	return val;
}

int8_t queue_is_empty(Queue *q)
{
	return !(q->index_back - q->index_front);
}