#include "string_queue.h"
#include "string.h"
#include "stdio.h"

void print_mem(uint8_t *buffer, uint8_t size);

void print_mem(uint8_t *buffer, uint8_t size)
{
	uint8_t align = 20;
	for(int i = 0; i < size;)
	{
		int j = 0;
		for(j = 0; j < align; j++)
		{
			if(i + j < size)
			{
				printf("0x%2.2X ", buffer[i + j]);
			}
			else
			{
				printf("     ");
			}
		}
		printf(" ");
		for(j = 0; j < align && i + j < size; j++)
		{
			if(buffer[i + j] != 0)
			{
				printf("%c", buffer[i + j]);
			}
			else
			{
				printf("%c", '.');
			}
		}
		printf("\n");
		i += j;
	}
}

void string_queue_init(string_queue *queue, uint8_t *buffer, uint16_t size)
{
	queue->begin = 0;
	queue->end = 0;
	queue->buffer = buffer;
	queue->size = size;
	memset(buffer, 0, size);
}

uint8_t string_queue_enqueue(string_queue *queue, const char *str, uint16_t size)
{
	//printf("enqueue %i %i\n", queue->begin, queue->end);
	//print_mem(queue->buffer, queue->size);

	if(string_queue_mem_left(queue) < size)
	{
		return -1;
	}
	
	uint16_t s = queue->size - queue->end;
	
	if(string_queue_mem_left(queue) > size)
	{
		if(s > size)
		{
			memcpy(queue->buffer + queue->end, str, size);
			queue->end += size + 1;
			return 0;
		}
		//printf("%d, %d, %d\n", queue->begin, queue->end, queue->size - queue->end);
		if(s <= size)
		{
			memcpy(queue->buffer + queue->end, str, s);
			memcpy(queue->buffer, str + s, size - s);
			queue->end = size - s + 1;
			return 0;
		}
	}
	return -1;
}

uint16_t string_queue_dequeue(string_queue *queue, char *str)
{
	//printf("dequeue\n");
	//print_mem(queue->buffer, queue->size);
	
	uint16_t endl = 0;
	uint16_t i;
	uint8_t isd = 0;
	for(i = queue->begin; i < queue->size; i++)
	{
		if(*(queue->buffer + i) == 0)
		{
			endl = i;
			//printf("endl1 %i\n", endl);
			break;
		}
	}
	if(i == queue->size)
	{
		isd = 1;
		for(i = 0; i < queue->size; i++)
		{
			if(*(queue->buffer + i) == 0)
			{
				endl = i;
				//printf("endl2 %i\n", endl);
				break;
			}
		}
	}
	
	if(isd)
	{
		int d = queue->size - queue->begin;
		memcpy(str, queue->buffer + queue->begin, queue->size - queue->begin);
		memcpy(str + d, queue->buffer, endl);
		memset(queue->buffer + queue->begin, 0, queue->size - queue->begin);
		memset(queue->buffer, 0, endl);
	}
	else
	{
		memcpy(str, queue->buffer + queue->begin, endl - queue->begin);
		memset(queue->buffer + queue->begin, 0, endl - queue->begin);
	}
	
	//print_mem(queue->buffer, queue->size);
	//printf("begin: %d, end: %d, is_d: %d\n", queue->begin, endl, isd);
	queue->begin = endl + 1;
	return 0;
}

uint16_t string_queue_mem_left(string_queue *queue)
{
	if(queue->end < queue->begin)
	{
		return queue->begin - queue->end - 1;
	}
	else
	{
		return queue->size - (queue->end - queue->begin) - 1; 
	}
}

uint8_t string_queue_is_empty(string_queue *queue)
{
	if(queue->end == queue->begin || queue->end - queue->begin == 1)
	{
		return -1;
	}
	return 0;
}