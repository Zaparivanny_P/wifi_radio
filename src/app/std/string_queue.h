#ifndef STRING_QUEUE_H
#define STRING_QUEUE_H

#include "stdint.h"


typedef struct string_queue
{
	uint16_t end;
	uint16_t begin;
	uint16_t size;
	uint8_t *buffer;
}string_queue;


void string_queue_init(string_queue *queue, uint8_t *buffer, uint16_t size);
uint8_t string_queue_enqueue(string_queue *queue, const char *str, uint16_t size);
uint16_t string_queue_dequeue(string_queue *queue, char *str);
uint16_t string_queue_mem_left(string_queue *queue);
uint8_t string_queue_is_empty(string_queue *queue);

#endif