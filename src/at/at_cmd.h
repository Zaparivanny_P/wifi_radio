#ifndef AT_CMD_H
#define AT_CMD_H
#include "stm32f10x.h"

typedef enum AT_CMD
{
	AT = 0,
	AT_RST,
	AT_GMR,
	AT_GSPL,
	ATE,
	AT_RESTORE,
	AT_UART,
	AT_UART_CUR,
	AT_UART_DEF,
	AT_SLEEP,
	AT_WAKEUPGPIO,
	AT_RFPOWER,
	AT_REFVDD,
	
	AT_CWMODE,
	AT_CWMODE_CUR,
	AT_CWMODE_DEF,
	AT_CWJAP,
	AT_CWJAP_CUR,
	AT_CWJAP_DEF,
	AT_CWLAPORT,
	AT_CWLAP,
	AT_CWQAP,
	AT_CWSAP,
	AT_CWSAP_CUR,
	AT_CWSAP_DEF,
	AT_CWLIF,
	AT_CWDHCP,
	AT_CWDHCP_CUR,
	AT_CWDHCP_DEF,
	AT_CWAUTOCONN,
	AT_CIPSTAMAC,
	AT_CIPSTAMAC_CUR,
	AT_CIPSTAMAC_DEF,
	AT_CIPAPMAC,
	AT_CIPAPMAC_CUR,
	AT_CIPAPMAC_DEF,
	AT_CIPSTA,
	AT_CIPSTA_CUR,
	AT_CIPSTA_DEF,
	AT_CIPAP,
	AT_CIPAP_CUR,
	AT_CIPAP_DEF,
	AT_CWSTARTSMART,
	AT_CWSTOPSTART,
	AT_CWSTARTDISCOVER,
	AT_CWSTOPDISCOVER,
	AT_WPS,
	AT_MDNS,
    AT_CWHOSTNAME,
	
	AT_CIPSTATUS,
	AT_CIPDOMAIN,
	AT_CIPSTART,
	AT_CIPSSLSIZE,
	AT_CIPSEND,
	AT_CIPSENDEX,
	AT_CIPSENDBUF,
	AT_CIPBUFSTATUS,
	AT_CIPCHECKSEQ,
	AT_CIPBUFRESET,
	AT_CIPCLOSE,
	AT_CIFSR,
	AT_CIPMUX,
	AT_CIPSERVER,
	AT_CIPMODE,
	AT_SAVETRANSLINK,
	AT_CIPSTO,
	AT_PING,
	AT_CIUPDATE,
	AT_CIPDINFO,
	
}AT_CMD;

typedef enum CW_MODE
{
	CW_M_SM = 1,
	CW_M_AP = 2,
	CW_M_AP_SM = 3,
}CW_MODE;

typedef enum WiFi_status
{
    WIFI_BUSY = 0,
    WIFI_OK,
    WIFI_FAIL,
    WIFI_ERROR,
    WIFI_READY_WRITE,
    WIFI_CLOSED,
}WiFi_status;

typedef struct WiFi_reply
{
    volatile WiFi_status status;
    AT_CMD cmd;
    uint32_t value1;
    void* ptr;
}WiFi_reply;

typedef struct
{  
  uint8_t       channel_num;
  int           rssi;                     
  char          ssid[30];  
  char          mac[18]; 
  uint8_t sec_type;
}wifi_scan;

typedef enum
{
  None          = 0, 
  WEP           = 1,
  WPA_Personal  = 2,
} WiFi_Priv_Mode;

typedef enum
{
	WIFI_TCP = 0,
	WIFI_UDP,
    WIFI_SSL,
}WiFi_socket_type;

typedef enum
{
    WIFI_OPEN = 0,
    WIFI_PSK = 2,
    WPA_PSK = 3,
    WPA_WPA2_PSK = 4,
}WiFi_encryption_method;

uint8_t at_parse(char *cmd, uint16_t length);


void esp_versions(void);

void wifi_set_cw_mode(WiFi_reply* reply, CW_MODE mode);
void wifi_network_scan(WiFi_reply* reply, wifi_scan *scan_result, uint16_t max_scan_number);
void wifi_connect(WiFi_reply* reply, char *ssid, char *sec_key, WiFi_Priv_Mode priv_mode);
void wifi_disconnect(WiFi_reply* reply);

void wifi_set_hostmane(WiFi_reply* reply, char *hostname);
/*********AP***********/

void wifi_configure_softap(WiFi_reply* reply, char *ssid, char *pwd, uint8_t chl, WiFi_encryption_method ech);
void wifi_set_ip_server(WiFi_reply* reply, char *ip, char *gateway, char *netmask);
void wifi_create_server(WiFi_reply* reply, uint16_t port);
void wifi_delete_server(WiFi_reply* reply, uint16_t port);
/*********TCP***********/

void wifi_socket_client_open(WiFi_reply* reply,  uint8_t id, uint8_t * hostname, uint32_t port_number, WiFi_socket_type type);
void wifi_socket_client_close(WiFi_reply* reply, uint8_t id);
void wifi_socket_client_write(WiFi_reply* reply, uint8_t id, char* data, uint32_t length);
void wifi_socket_enabled_mux(WiFi_reply* reply, uint8_t is_enable);

void wifi_socket_client_write_data(WiFi_reply* reply);
//void wifi_socket_client_start_write(WiFi_reply* reply, uint8_t id, uint32_t length);
//void wifi_socket_client_block_write(WiFi_reply* reply, char *msg, uint32_t length);

/*************STATUS NO WIFI***************/
uint8_t wifi_socket_mux_is_enable(void);
/****************************/
void wifi_network_scan_parse(WiFi_reply* reply, char *buffer);

/***********signal******************/
void wifi_tcp_signal_connect(uint8_t id);
void wifi_tcp_signal_closed(uint8_t id);


#endif
