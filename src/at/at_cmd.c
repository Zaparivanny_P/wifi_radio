#include "at_cmd.h"
#include "stdio.h"
#include "string.h"
#include "at_hardware.h"
#include "application.h"
#include "stdlib.h"

#include "audio_stream.h"

char at_buffer[1024];
char resp_arg_buff[1024];

WiFi_reply* c_reply = NULL;
static uint8_t esp_mux_mode = 0;
static uint8_t is_responsr_parse = 1;

char *at_cmd_resp[] = { "%s", 
                        "%s",
                        "%s"};



static const char *at_cmd_request[] = { "AT\r\n", 
                                        "AT+RST\r\n",
                                        "AT+GMR\r\n", "", "", "", "", "", "", "", "", "", "",
                                        "AT+CWMODE=%i\r\n",
                                        "", "", "", 
                                        "AT+CWJAP_CUR=\"%s\",\"%s\"\r\n", "", "",
                                        "AT+CWLAP\r\n",
                                        "AT+CWQAP\r\n", 
                                        "AT+CWSAP=\"%s\",\"%s\",%i,%i\r\n", 
                                        "AT+CWSAP_CUR=\"%s\",\"%s\",%i,%i\r\n", 
                                        "AT+CWSAP_DEF=\"%s\",\"%s\",%i,%i\r\n", 
                                        "", "", "", "", "", "", "", "", "", "", "",
                                        "AT+CIPSTA=\"%s\",\"%s\",\"%s\"\r\n", 
                                        "AT+CIPSTA_CUR=\"%s\",\"%s\",\"%s\"\r\n", 
                                        "AT+CIPSTA_DEF=\"%s\",\"%s\",\"%s\"\r\n", 
                                        "AT+CIPAP=\"%s\",\"%s\",\"%s\"\r\n", 
                                        "AT+CIPAP_CUR=\"%s\",\"%s\",\"%s\"\r\n", 
                                        "AT+CIPAP_DEF=\"%s\",\"%s\",\"%s\"\r\n", 
                                        "", "", "", "", "", "", 
                                        "AT+CWHOSTNAME=\"%s\"\r\n", 
                                        "", "",
                                        "AT+CIPSTART", "",
                                        "AT+CIPSEND",
                                        "", "", "", "", "", 
                                        "AT+CIPCLOSE" , "",
                                        "AT+CIPMUX=%i\r\n",
                                        "AT+CIPSERVER=%i,%i\r\n"};

WiFi_status at_is_complite(char *cmd, uint16_t length)
{
    int p;
    //static int c = 0;
    //if(++c == 3)
    {
		char *ptr = strstr(cmd, "+IPD");
		if(ptr)
		{
			asm("nop");
		}
        
        p = memcmp(cmd + length - 3, "OK\r\n", 4);
        if(p == 0) return WIFI_OK;
        p = memcmp(cmd + length - 5, "FAIL\r\n", 6);
        if(p == 0) return WIFI_FAIL;
        p = memcmp(cmd + length - 6, "ERROR\r\n", 7);
        if(p == 0) return WIFI_ERROR;
    }
	return WIFI_BUSY;
}
										
uint8_t at_parse(char *cmd, uint16_t length)
{
    /*if(!is_responsr_parse)
    {
        char *ptr = strstr(cmd, "SEND OK");
        if(ptr)
        {
            c_reply->status = WIFI_OK;
            app_obj_signal_emit(c_reply);
        }
        return WIFI_OK;
    }*/
    /*********CONNECT STATUS*********/
    char *ch2 = strpbrk(cmd, ",");
    if(ch2)
    {
        int idch = -1;
        int res = memcmp(ch2 + 1, "CONNECT", 7);
        if(res == 0)
        {
            sscanf(cmd, "%i", &idch);
            wifi_tcp_signal_connect(idch);
            //TODO emit connect
            return WIFI_BUSY;
        }
        res = memcmp(ch2 + 1, "CLOSED", 6);
        if(res == 0)
        {
            sscanf(cmd, "%i", &idch);
            wifi_tcp_signal_closed(idch);
            // TODO emit closed
            return WIFI_BUSY;
        }
    }
    
    /*********CMD STATUS************/
    WiFi_status wifi_status = at_is_complite(cmd, length);
	if(wifi_status != WIFI_BUSY && c_reply)
	{
        //char *template_parse = at_cmd_resp[c_reply->cmd];
		c_reply->status = wifi_status;
        cmd += strlen(at_cmd_request[AT_GMR]);
		switch(c_reply->cmd)
		{
			case AT_CWLAP:
				wifi_network_scan_parse(c_reply, cmd);
			break;
            case AT_CIPSEND:
				if(c_reply->ptr)
				{
					wifi_socket_client_write_data(c_reply);
					c_reply->status = WIFI_BUSY;
					wifi_status = WIFI_BUSY;
					c_reply->ptr = NULL;
				}
            break;
			case AT_CIPMUX:
				if(c_reply->status == WIFI_OK)
				{
					esp_mux_mode = c_reply->value1;
				}
			break;
			default:
			//int res = sscanf(cmd, template_parse, resp_arg_buff);
		}

        if(c_reply->status != WIFI_BUSY)
        {
            app_obj_signal_emit(c_reply);
            c_reply = NULL;
        }
	}
	return wifi_status;
}

void esp_versions(void)
{
    
}

void wifi_reply_init(WiFi_reply* reply, AT_CMD cmd)
{
    c_reply = reply;
    reply->status = WIFI_BUSY;
    reply->cmd = cmd;
    is_responsr_parse = 1;
}

void wifi_set_cw_mode(WiFi_reply* reply, CW_MODE mode)
{
    wifi_reply_init(reply, AT_CWMODE);
    sprintf(at_buffer, at_cmd_request[AT_CWMODE], mode);
    uint16_t length = strlen(at_buffer);
    at_send(at_buffer, length);
}

void wifi_network_scan(WiFi_reply* reply, wifi_scan *scan_result, uint16_t max_scan_number)
{
    wifi_reply_init(reply, AT_CWLAP);
    reply->value1 = max_scan_number;
    reply->ptr = scan_result;
    sprintf(at_buffer, at_cmd_request[AT_CWLAP]);
    uint16_t length = strlen(at_buffer);
    at_buffer[length] = 0;
    at_send(at_buffer, length);
}

void wifi_connect(WiFi_reply* reply, char *ssid, char *sec_key, WiFi_Priv_Mode priv_mode)
{
    wifi_reply_init(reply, AT_CWJAP_CUR);
    sprintf(at_buffer, at_cmd_request[AT_CWJAP_CUR], ssid, sec_key);
    uint16_t length = strlen(at_buffer);
    at_send(at_buffer, length);
}

void wifi_disconnect(WiFi_reply* reply)
{
    wifi_reply_init(reply, AT_CWQAP);
    sprintf(at_buffer, at_cmd_request[AT_CWQAP]);
    uint16_t length = strlen(at_buffer);
    at_send(at_buffer, length);
}

void wifi_set_hostmane(WiFi_reply* reply, char *hostname)
{
    wifi_reply_init(reply, AT_CWHOSTNAME);
    sprintf(at_buffer, at_cmd_request[AT_CWHOSTNAME], hostname);
    uint16_t length = strlen(at_buffer);
    at_send(at_buffer, length);
}

void wifi_configure_softap(WiFi_reply* reply, char *ssid, char *pwd, uint8_t chl, WiFi_encryption_method ech)
{
    wifi_reply_init(reply, AT_CWSAP_CUR);
    sprintf(at_buffer, at_cmd_request[AT_CWSAP_CUR], ssid, pwd, chl, ech);
    uint16_t length = strlen(at_buffer);
    at_send(at_buffer, length);
}

void wifi_set_ip_server(WiFi_reply* reply, char *ip, char *gateway, char *netmask)
{
    wifi_reply_init(reply, AT_CIPAP_CUR);
    sprintf(at_buffer, at_cmd_request[AT_CIPAP_CUR], ip, gateway, netmask);
    uint16_t length = strlen(at_buffer);
    at_send(at_buffer, length);
}

void wifi_create_server(WiFi_reply* reply, uint16_t port)
{
    wifi_reply_init(reply, AT_CIPSERVER);
    sprintf(at_buffer, at_cmd_request[AT_CIPSERVER], 1, port);
    uint16_t length = strlen(at_buffer);
    at_send(at_buffer, length);
}

void wifi_delete_server(WiFi_reply* reply, uint16_t port)
{
    wifi_reply_init(reply, AT_CIPSERVER);
    sprintf(at_buffer, at_cmd_request[AT_CIPSERVER], 0, port);
    uint16_t length = strlen(at_buffer);
    at_send(at_buffer, length);
}

void wifi_socket_client_open(WiFi_reply* reply, uint8_t id, uint8_t * hostname, uint32_t port_number, WiFi_socket_type type)
{
    wifi_reply_init(reply, AT_CIPSTART);
    static char *contype[] = {"TCP", "UDP", "SSL"};
    if(wifi_socket_mux_is_enable())
    {
        sprintf(at_buffer, "%s=%i,\"%s\",\"%s\",%i\r\n", at_cmd_request[AT_CIPSTART], id, contype[0], hostname, port_number);
    }
    else
    {
        sprintf(at_buffer, "%s=\"%s\",\"%s\",%i\r\n", at_cmd_request[AT_CIPSTART], contype[type], hostname, port_number);
    }
    uint16_t length = strlen(at_buffer);
    at_send(at_buffer, length);
}

void wifi_socket_client_close(WiFi_reply* reply, uint8_t id)
{
    wifi_reply_init(reply, AT_CIPCLOSE);

    if(wifi_socket_mux_is_enable())
    {
            sprintf(at_buffer, "%s=%i\r\n", at_cmd_request[AT_CIPCLOSE], id);
    }
    else
    {
            sprintf(at_buffer, "%s\r\n", at_cmd_request[AT_CIPCLOSE]);
    }
    uint16_t length = strlen(at_buffer);
    at_send(at_buffer, length);
}

void wifi_socket_client_write(WiFi_reply* reply, uint8_t id, char* data, uint32_t length)
{
    wifi_reply_init(reply, AT_CIPSEND);
    reply->ptr = data;
    reply->value1 = length;
    if(wifi_socket_mux_is_enable())
    {
        sprintf(at_buffer, "%s=%i,%u\r\n", at_cmd_request[AT_CIPSEND], id, length);
    }
    else
    {
        sprintf(at_buffer, "%s=%u\r\n", at_cmd_request[AT_CIPSEND], length);
    }
    uint16_t slength = strlen(at_buffer);
    at_send(at_buffer, slength);
}

void wifi_socket_enabled_mux(WiFi_reply* reply, uint8_t is_enable)
{
	wifi_reply_init(reply, AT_CIPMUX);
	reply->value1 = is_enable ? 1 : 0;
	sprintf(at_buffer, at_cmd_request[AT_CIPMUX], reply->value1);
    uint16_t slength = strlen(at_buffer);
	at_send(at_buffer, slength);
}

void wifi_socket_client_write_data(WiFi_reply* reply)
{
    c_reply = reply;
    sprintf(at_buffer, "%s", reply->ptr);
    at_send(at_buffer, reply->value1);
}

uint8_t wifi_socket_mux_is_enable(void)
{
	return esp_mux_mode;
}

/************PRIVATE**************/

void wifi_network_scan_parse(WiFi_reply* reply, char *buffer)
{
	wifi_scan *scan_result = reply->ptr;
	char * entry = NULL;
    uint8_t cnt = 0;
	int w, num;
	while(1)
	{
		entry = strstr(buffer, "+CWLAP:");
        if(entry == NULL)
        {
            break;
        }
		entry += 8;
        sscanf(entry, "%i,%[^','],%i,%[^','],%i", &w, scan_result[cnt].ssid, &scan_result[cnt].rssi, 
               scan_result[cnt].mac, &num);
	    scan_result[cnt].channel_num = num;
		scan_result[cnt].sec_type = w;
        cnt++;
		buffer = entry;
	}
	
}

__weak void wifi_tcp_signal_connect(uint8_t id)
{
    
}

__weak void wifi_tcp_signal_closed(uint8_t id)
{
    
}