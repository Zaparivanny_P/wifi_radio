#include "wifi_list_radio.h"
#include "application.h"
#include "at_cmd.h"
#include "stdlib.h"
#include "string.h"

#define LINKID_REPO 2

void wifi_repository_reply_list(void *obj)
{
	app_obj_disconnect_signal(obj);
	free(obj);
}

void wifi_repository_connected(void *obj)
{
	app_obj_disconnect_signal(obj);
	
	static char responce[] = "GET /Zaparivanny_P/wifi_radio/downloads/radio.json HTTP/1.1\r\n"
							 "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.21 Safari/537.36 MMS/1.0.2531.0\r\n"
							 "Host: bitbucket.org\r\n"
							 "Cache-Control: no-cache\r\n\r\n\0";
	
	
	WiFi_reply *reply_obj = obj;
    if(reply_obj->status == WIFI_OK)
    {
        WiFi_reply *reply = malloc(sizeof(WiFi_reply));
        app_obj_connect(reply, wifi_repository_reply_list, CONNECT_DIRECT);
        wifi_socket_client_write(reply, LINKID_REPO, responce, strlen(responce));
    }
	
    free(obj);
}

void wifi_list_download(void)
{
	WiFi_reply *reply = malloc(sizeof(WiFi_reply));
	app_obj_connect(reply, wifi_repository_connected, CONNECT_DIRECT);
	wifi_socket_client_open(reply, LINKID_REPO, "bitbucket.org\0", 443, WIFI_SSL);
}

