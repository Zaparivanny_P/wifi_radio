#include "at_frame_manager.h"
#include "audio_stream.h"
#include "string.h"
#include "application.h"
#include "webserver.h"

#define IPD_BUFFER 1024

static char ipd_buffer[IPD_BUFFER];

static uint16_t ipd_cnt = 0;
static uint16_t g_id = 0;
static uint32_t g_length = 0;

void (*receive_byte)(uint8_t data) = NULL;

void (*at_callback_write)(uint8_t data) = NULL;
uint8_t (*at_callback_create)(uint32_t length) = NULL;
static int16_t c_id = -1;

void wifi_receive_byte_stream(uint8_t data);
void wifi_receive_byte_data(uint8_t data);

static uint8_t ipd_cnt_frame_debug = 0;

void at_frame_register_callback(uint8_t id, void (*callback_write)(uint8_t data), uint8_t (*callback_create)(uint32_t length))
{
	at_callback_write = callback_write;
	at_callback_create = callback_create;
	//c_id = id;
}

void at_frame_manager_complite(void)
{
	
}

uint16_t at_manager_id(void)
{
	return g_id;
}

char* at_manager_buffer(void)
{
	return ipd_buffer;
}

void wifi_receive_frame(uint16_t id, uint32_t length)
{
	g_id = id;
	g_length = length;
	if(id == c_id)
	{
		if(at_callback_create)
		{
			uint8_t res = at_callback_create(g_length);
			receive_byte = res == 0 ? NULL : at_callback_write;
		}
	}
	else
	{
		ipd_cnt = 0;
        memset(ipd_buffer, 0, sizeof(ipd_buffer));
		receive_byte = wifi_receive_byte_data;
	}
}

void wifi_receive_byte(uint8_t data)
{
	if(receive_byte)
	{
		receive_byte(data);
	}
}

void wifi_receive_byte_data(uint8_t data)
{
	if(ipd_cnt < IPD_BUFFER)
	{
		ipd_buffer[ipd_cnt++] = data;
		if(g_length == ipd_cnt)
		{
            ipd_cnt_frame_debug++;
			//app_obj_signal_emit((void*)at_frame_manager_complite);
            web_server_read();
		}
	}
}
