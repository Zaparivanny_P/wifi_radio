#include "wifi_example.h"
#include "application.h"
#include "at_cmd.h"
#include "stdlib.h"
#include "stdio.h"
#include "wifi_list_radio.h"
#include "string.h"
#include "board.h"

#define LINK_AUDIO_ID 1

typedef struct Radio_Node
{
    uint8_t *host;
    uint8_t *chanel;
    uint32_t port;
}Radio_Node;

Radio_Node radio_node[7];
uint8_t default_radio = 6;

void radio_nodes_init(void)
{
    static char *radio[] = {"ice1.somafm.com\0", 
                            "media.govoritmoskva.ru\0", 
                            "stream04.media.rambler.ru\0",
                            "ic3.101.ru\0",
                            "nashe1.hostingradio.ru\0",
                            "icecast.radiomaximum.cdnvideo.ru\0",
                            "icecast.radiomaximum.cdnvideo.ru\0"};
    static char *radio_ch[] = {"u80s-256-mp3\0", 
                               "rufm_m.mp3\0", 
                               "megapolis128.mp3\0",
                               "v12_1\0",
                               "jazz-256\0",
                               "maximum.mp3\0",
                               "st25.mp3\0"};
    uint32_t ports[] = {80, 8000, 80, 8000, 80, 80, 80};
    
    
    for(uint8_t i = 0; i < sizeof(radio_node) / sizeof(radio_node[0]); i++)
    {
        radio_node[i].host = radio[i];
        radio_node[i].chanel = radio_ch[i];
        radio_node[i].port = ports[i];
    }
}

void wifi_tcp_wtite_slot(void *obj)
{
    app_obj_disconnect_signal(obj);
    free(obj);
}

void wifi_tcp_connect_slot(void *obj)
{
    WiFi_reply *reply_obj = obj;
    if(reply_obj ->status != WIFI_OK)
    {
        _delay(10000);
        reply_obj->status = WIFI_OK;
        wifi_connect_slot(obj);
        return;
    }
	app_obj_disconnect_signal(obj);
	//http://stream04.media.rambler.ru/megapolis128.mp3 
    
	/*static char responce2[] = "GET /u80s-128-mp3 HTTP/1.1\r\n"
							 "Icy-MetaData: 0\r\n"
							 "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.21 Safari/537.36 MMS/1.0.2531.0\r\n"
							 "Host: ice1.somafm.com\r\n"
							 "Cache-Control: no-cache\r\n\r\n\0";
     
    static char responce1[] = "GET /rufm_m.mp3 HTTP/1.1\r\n"
							 "Icy-MetaData: 0\r\n"
							 "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.21 Safari/537.36 MMS/1.0.2531.0\r\n"
							 "Host: media.govoritmoskva.ru\r\n"
							 "Cache-Control: no-cache\r\n\r\n\0";
    
    static char responce[] = "GET /megapolis128.mp3 HTTP/1.1\r\n"
							 "Icy-MetaData: 0\r\n"
							 "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.21 Safari/537.36 MMS/1.0.2531.0\r\n"
							 "Host: stream04.media.rambler.ru\r\n"
							 "Cache-Control: no-cache\r\n\r\n\0";*/
    static char responce[] = "GET /%s HTTP/1.1\r\n"
							 "Icy-MetaData: 0\r\n"
							 "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.21 Safari/537.36 MMS/1.0.2531.0\r\n"
							 "Host: %s\r\n"
							 "Cache-Control: no-cache\r\n\r\n";
    static char buffer[512];
    sprintf(buffer, responce, radio_node[default_radio].chanel, radio_node[default_radio].host);
	
	
    if(reply_obj->status == WIFI_OK)
    {
        WiFi_reply *reply = malloc(sizeof(WiFi_reply));
        app_obj_connect(reply, wifi_tcp_wtite_slot, CONNECT_DIRECT);
        wifi_socket_client_write(reply, LINK_AUDIO_ID, buffer, strlen(buffer));
    }
    else
    {
#ifndef NDEBUG
        printf("WIFI_ERROR connect %i\n", reply_obj->status);
#endif
    }
	
    free(obj);
}

void wifi_connect_slot(void *obj)
{
    app_obj_disconnect_signal(obj);
    
    /*WiFi_reply *reply_obj = obj;
    if(reply_obj ->status == WIFI_OK)
    {
        WiFi_reply *reply = malloc(sizeof(WiFi_reply));
        app_obj_connect(reply, wifi_tcp_connect_slot, CONNECT_DIRECT);
        wifi_socket_client_open(reply, LINK_AUDIO_ID, radio_node[default_radio].host, radio_node[default_radio].port, WIFI_TCP);
    }*/
    //wifi_list_download();
    free(obj);
}

void wifi_mux_enable(void *obj)
{
    app_obj_disconnect_signal(obj);
	
    WiFi_reply *reply_obj = obj;
    if(reply_obj->status == WIFI_OK)
    {
        WiFi_reply *reply = malloc(sizeof(WiFi_reply));
        app_obj_connect(reply, wifi_set_mode, CONNECT_DIRECT);
        wifi_socket_enabled_mux(reply, 1);
    }
    else
    {
#ifndef NDEBUG
        printf("WIFI_ERROR scan %i\n", reply_obj->status);
#endif
    }
    
    free(obj);
}

void wifi_scan_slot(void *obj)
{
    app_obj_disconnect_signal(obj);
	
    WiFi_reply *reply_obj = obj;
    if(reply_obj->status == WIFI_OK)
    {
        WiFi_reply *reply = malloc(sizeof(WiFi_reply));
        app_obj_connect(reply, wifi_connect_slot, CONNECT_DIRECT);
        //wifi_connect(reply, "Keenetic-1172\0", "integralbeta\0", WEP);
        wifi_connect(reply, "ELTEX-2.4GHz_WiFi_9DC8\0", "GP2A000681\0", WEP);
    }
    else
    {
#ifndef NDEBUG
        printf("WIFI_ERROR scan %i\n", reply_obj->status);
#endif
    }
    
    free(obj);
}

void wifi_slot(void *obj)
{
    static wifi_scan scan_result[20];
    app_obj_disconnect_signal(obj);
    WiFi_reply *reply_obj = obj;
    if(reply_obj->status == WIFI_OK)
    {
        WiFi_reply *reply = malloc(sizeof(WiFi_reply));
        app_obj_connect(reply, wifi_scan_slot, CONNECT_DIRECT);
        wifi_network_scan(reply, scan_result, 20);
    }
    else
    {
#ifndef NDEBUG
        printf("WIFI_ERROR set mode %i\n", reply_obj->status);
#endif
    }
    free(obj);
}

void wifi_set_mode(void *obj)
{
    radio_nodes_init();
    app_obj_disconnect_signal(obj);
    WiFi_reply* reply = malloc(sizeof(WiFi_reply));
    app_obj_connect(reply, wifi_ex_set_ip_softap, CONNECT_DIRECT);
    //app_obj_connect(reply, wifi_slot, CONNECT_DIRECT);
    wifi_set_cw_mode(reply, CW_M_AP_SM);
    free(obj);
}

void wifi_ex_set_ip_softap(void *obj)
{
    app_obj_disconnect_signal(obj);
    WiFi_reply* reply = malloc(sizeof(WiFi_reply));
    app_obj_connect(reply, wifi_ex_create_server, CONNECT_DIRECT);
    wifi_set_ip_server(reply, "192.168.5.1", "192.168.5.1", "255.255.255.0");
    free(obj);
}

void wifi_ex_create_server(void *obj)
{
    app_obj_disconnect_signal(obj);
    WiFi_reply* reply = malloc(sizeof(WiFi_reply));
    app_obj_connect(reply, wifi_slot, CONNECT_DIRECT);
    wifi_create_server(reply, 80);
    free(obj);
}

void wifi_ex_configure_softap(void *obj)
{
    app_obj_disconnect_signal(obj);
    WiFi_reply* reply = malloc(sizeof(WiFi_reply));
    app_obj_connect(reply, wifi_mux_enable, CONNECT_DIRECT);
    wifi_configure_softap(reply, "wifi_radio", "", 5, WIFI_OPEN);
    free(obj);
}

void wifi_ex_disconnect()
{
    WiFi_reply* reply = malloc(sizeof(WiFi_reply));
    app_obj_connect(reply, wifi_ex_configure_softap, CONNECT_DIRECT);
    wifi_disconnect(reply);
}

void wifi_example(void)
{
    wifi_ex_disconnect();
}