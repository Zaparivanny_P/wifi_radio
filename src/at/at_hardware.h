#ifndef AT_HARDWARE_H
#define AT_HARDWARE_H
#include "stm32f10x.h"

void at_send(char* buff, uint16_t length);

__weak void wifi_receive_frame(uint16_t id, uint32_t length);
__weak void wifi_receive_byte(uint8_t data);

#endif