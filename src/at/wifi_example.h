#ifndef WIFI_EXAMPLE_H
#define WIFI_EXAMPLE_H

void wifi_ex_set_ip_softap(void *obj);
void wifi_set_mode(void *obj);
void wifi_slot(void *obj);
void wifi_scan_slot(void *obj);
void wifi_mux_enable(void *obj);
void wifi_connect_slot(void *obj);
void wifi_tcp_connect_slot(void *obj);
void wifi_tcp_wtite_slot(void *obj);
void wifi_ex_create_server(void *obj);


void wifi_example(void);


#endif