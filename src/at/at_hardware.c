#include "at_hardware.h"
#include "stm32f10x.h"
#include "string.h"
#include "stdio.h"
#include "audio_stream.h"

#include "at_cmd.h"
#define AT_BUFFER_SIZE 2048

char at_buffer_rx[AT_BUFFER_SIZE];
uint32_t cnt = 0;
uint8_t mode = 0;

uint8_t ipd_cnt_debug = 0;
uint8_t ipd_id_buffer_debug[20] = {-1, -1, -1, -1, -1,
                                   -1, -1, -1, -1, -1,
                                   -1, -1, -1, -1, -1,
                                   -1, -1, -1, -1, -1};

void at_send(char* buff, uint16_t length)
{
    //USART_SendData(USART1, 0xAD);
    
    DMA_InitTypeDef DMA_InitStructure;
    
    DMA_ClearFlag(DMA1_FLAG_TC4 | DMA1_FLAG_TE4 | DMA1_FLAG_HT4 | DMA1_FLAG_GL4);
    USART_ClearFlag(USART1, USART_FLAG_TC);
    USART_ClearFlag(USART1, USART_FLAG_TXE);

    DMA_ClearITPendingBit(DMA1_IT_GL4);

    DMA_Cmd(DMA1_Channel4, DISABLE);
    
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART1->DR;
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)buff;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_BufferSize = length;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel4, &DMA_InitStructure);

    //cnt = 0;
    //mode = 0;
    DMA_Cmd(DMA1_Channel4, ENABLE);
       
}

uint16_t fast_read_int(char *buff, char delim)
{
	uint16_t v = 0;
	uint8_t num_cnt = 0;
	char ch = buff[num_cnt];
    num_cnt++;
	do
	{
		v *= 10;
		v += (ch - 0x30);
		ch = buff[num_cnt];
        num_cnt++;
	}while(ch != delim);
	return v;
}

void USART1_IRQHandler()
{
	
    if(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == SET)
    {
        uint8_t value = USART_ReceiveData(USART1);
        at_buffer_rx[cnt] = value;
        USART_ClearFlag(USART1, USART_FLAG_RXNE);
        
        if(++cnt >= AT_BUFFER_SIZE)
        {
            cnt = 0;
        }
		
        static int v = 0;
        static uint8_t state = 0;
        static uint16_t ipd_length = 0;
        static uint16_t ipd_cnt = 0;
		static uint16_t ipd_id = 0;
        
        if(cnt >= 4 && state == 0 && 
            at_buffer_rx[cnt - 4] == '+' &&
            at_buffer_rx[cnt - 3] == 'I' &&
            at_buffer_rx[cnt - 2] == 'P' &&
            at_buffer_rx[cnt - 1] == 'D')
        {
            cnt = 0;
            mode = 0;
			if(wifi_socket_mux_is_enable())
			{
				state = 1;
			}
			else
			{
				state = 3;
			}
            ipd_cnt_debug++;
        }
        
		switch(state)
		{
			case 4:
			
				wifi_receive_byte(value);
				if(++ipd_cnt == ipd_length)
				{
					v++;
					state = 0;
                    cnt = 0;
				}
				break;
			case 3:
			
				if(value == ':')
				{
					ipd_length = 0;
					ipd_cnt = 0;
					ipd_length = fast_read_int(at_buffer_rx, ':');
					state = 4;
					cnt = 0;
					
					wifi_receive_frame(ipd_id, ipd_length);
				}
				break;
			case 1:
				if(value == ',')
				{
					state = 2;
					cnt = 0;
				}
				break;
			case 2:
				if(value == ',')
				{
					ipd_id = fast_read_int(at_buffer_rx, ',');
                    ipd_id_buffer_debug[ipd_cnt_debug - 1] = ipd_id;
					state = 3;
					cnt = 0;
				}
				break;
		}
        
        if(state == 0 && at_buffer_rx[cnt - 2] == '\r' && at_buffer_rx[cnt - 1] == '\n')
        {
            /*if(mode)
            {
                if(at_parse(at_buffer_rx, cnt - 1))
                {
                    
                    //memset(at_buffer_rx, 0, sizeof(at_buffer_rx));
                }
                mode = 0;
                cnt = 0;
            }
            else
            {
                if(at_buffer_rx[cnt - 4] == '\r' && at_buffer_rx[cnt - 3] == '\n')
                {
                    mode = 1;
                }
            }*/
            if(cnt > 2)
            {
                at_buffer_rx[cnt] = 0; //TODO KOSTIL
                at_parse(at_buffer_rx, cnt - 1);
            }
            cnt = 0;
        }
    
    }
    else
    {
        USART_ClearFlag(USART1, USART_FLAG_TC);
        asm("nop");
    }
}



__weak void wifi_receive_frame(uint16_t id, uint32_t length)
{
	
}

__weak void wifi_receive_byte(uint8_t data)
{
	
}
