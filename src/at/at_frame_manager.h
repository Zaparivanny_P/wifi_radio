#ifndef AT_FRAME_MANAGER_H
#define AT_FRAME_MANAGER_H

#include "stdint.h"

void at_frame_register_callback(uint8_t id, void (*callback_write)(uint8_t data), uint8_t (*callback_create)(uint32_t length));

void at_frame_manager_complite(void);
uint16_t at_manager_id(void);
char* at_manager_buffer(void);

#endif