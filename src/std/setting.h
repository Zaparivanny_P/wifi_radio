#ifndef SETTING_H
#define SETTING_H

#include "list_legacy.h"
#include "stdbool.h"

typedef struct Setting
{
	List list;
}Setting;

void setting_create(Setting *setting);
bool setting_parse(Setting *setting, uint8_t* str);
uint8_t* setting_get_key(Setting *setting, uint8_t* key);
void setting_free(Setting *setting);


#endif