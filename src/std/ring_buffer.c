#include "ring_buffer.h"

void rign_buffer_init(Ring_buffer *buffer, uint8_t *buff, uint32_t length)
{
	buffer->buffer = buff;
	buffer->length = length;
	buffer->last = buff;
	buffer->first = buff;
	buffer->free_space = length;
}

uint8_t rign_buffer_write(Ring_buffer *buffer, uint8_t data)
{
	if(buffer->free_space == 0)
	{
		return 0;
	}
	
	*buffer->last = data;
	buffer->last++;
	if((int)buffer->last == (int)(buffer->length + buffer->buffer))
	{
		buffer->last = buffer->buffer;
	}
	buffer->free_space--;
	return 1;
}

uint8_t rign_buffer_read_u8(Ring_buffer *buffer)
{
	if(rign_buffer_available_byte(buffer) == 0)
	{
		return 0;
	}
	
	uint8_t data = *buffer->first;
	*buffer->first = 0;
	buffer->free_space++;
	if(++buffer->first == buffer->length + buffer->buffer)
	{
		buffer->first = buffer->buffer;
	}
	return data;
}

uint8_t rign_buffer_read_offset(Ring_buffer *buffer, uint32_t offset)
{
	if(rign_buffer_available_byte(buffer) < offset)
	{
		return 0;
	}
	uint8_t *ptr = buffer->first + offset;
	if(ptr >= buffer->length + buffer->buffer)
	{
		ptr -= buffer->length;
	}
	
	return *ptr;
}

uint32_t rign_buffer_available_space(Ring_buffer *buffer)
{
	return buffer->free_space;
}

uint32_t rign_buffer_available_byte(Ring_buffer *buffer)
{
    return buffer->length - buffer->free_space;
}