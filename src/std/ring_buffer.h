#ifndef RING_BUFFER_H
#define RING_BUFFER_H

#include "stdint.h"

typedef struct Ring_buffer
{
	uint8_t *buffer;
	uint32_t length;
	uint8_t *last;
	uint8_t *first;
	uint32_t free_space;
}Ring_buffer;

void rign_buffer_init(Ring_buffer *buffer, uint8_t *buff, uint32_t length);

uint8_t rign_buffer_write(Ring_buffer *buffer, uint8_t data);
uint8_t rign_buffer_read_u8(Ring_buffer *buffer);

uint8_t rign_buffer_read_offset(Ring_buffer *buffer, uint32_t offset);

uint32_t rign_buffer_available_space(Ring_buffer *buffer);
uint32_t rign_buffer_available_byte(Ring_buffer *buffer);

#endif