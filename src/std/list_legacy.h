#ifndef LIST_LEGACY_H
#define LIST_LEGACY_H

#include "stdint.h"
#include "stdbool.h"

struct ListItem;

typedef struct ListItem
{
	struct ListItem *prev;
	struct ListItem *next;
	void *item;
	uint32_t obj_size;
}ListItem;

typedef struct List
{
	ListItem *current;
	uint8_t size;
}List;

typedef struct ListIterator
{
	List list;
	ListItem *current;	
}ListIterator;

/*private function*/




/*public function*/
uint8_t list_push_back(List *list, void* ptr, uint8_t size);
void *list_get(List *list);
void *list_find(List *list, void* src, uint8_t size);

bool list_has_next(List *list);
bool list_has_prev(List *list);
bool list_next(List *list);
bool list_prev(List *list);
void list_begin(List *list);
void list_end(List *list);
void list_erase(List *list);

static inline uint8_t list_size(List *list)
{
	return list->size;
}

bool list_iterator(ListIterator *it, List *list);
bool list_iterator_next(ListIterator *it);
bool list_iterator_prev(ListIterator *it);
void *list_iterator_find(ListIterator *it, void* src, uint8_t size);

static inline void list_iterator_begin(ListIterator *it)
{
	//list_begin(&it->list);
	while(list_iterator_prev(it));
}

static inline void list_iterator_end(ListIterator *it)
{
	//list_end(&it->list);
	while(list_iterator_next(it));
}

static inline void *list_iterator_get(ListIterator *it)
{
    if(it->current == (void*)0)
        return (void*)0;
	return it->current->item;
}

void list_free(List *list);

#endif