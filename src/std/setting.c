#include "setting.h"
#include "stdlib.h"
#include "string.h"


void setting_create(Setting *setting)
{
	memset(&setting->list, 0, sizeof(List));
}

bool setting_parse(Setting *setting, uint8_t* str)
{
	list_push_back(&setting->list, str, strlen((char const*)str) + 1);
    return true;
}

int8_t _setting_size_key(uint8_t *str)
{
	for(uint8_t i = 0; i < strlen((char const*)str); i++)
	{
		if(str[i] == '=' || str[i] == ' ')
		{
			return i;
		}
	}
	return -1;
}

uint8_t* setting_get_key(Setting *setting, uint8_t* key)
{
	list_begin(&setting->list);
	do
	{
		uint8_t* str = list_get(&setting->list);
        static uint8_t len;
		
		int8_t key_len = _setting_size_key(str);
        
		if((key_len == -1) || (key_len != strlen((char const*)key)))
			continue;
		
		len = strncmp((char const*)str, (char const*)key, strlen((char const*)key));
		if(len == 0)
		{
            uint8_t cnt = strlen((char const*)key);
            for(; cnt < strlen((char const*)str); cnt++)
            {
                if(str[cnt] == '=')
                    break;
            }
            cnt++;
            for(; cnt < strlen((char const*)str); cnt++)
            {
                if(str[cnt] != ' ')
                    return str + cnt;
            }
			return NULL;
		}
	}while(list_next(&setting->list));
	return NULL;
}

void setting_free(Setting *setting)
{
	list_free(&setting->list);
}