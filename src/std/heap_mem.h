#ifndef HEAP_MEM_H
#define HEAP_MEM_H

void hm_malloc(uint8_t pntr, uint8_t size);
void hm_realloc(uint8_t size);
void hm_free(uint8_t size);


#endif