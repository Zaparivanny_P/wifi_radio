#include "list_legacy.h"
#include "string.h"
#include "stdlib.h"

uint8_t list_push_back(List *list, void* ptr, uint8_t size)
{
	void *p = malloc(size);// __iar_dlmalloc_stats();
	if(p == NULL && size != 0)
	{
		return -1;
	}
	
	ListItem *l = malloc(sizeof(ListItem));// __iar_dlmalloc_stats();
	if(l == NULL)
	{
		free(p); 
        return -1;
	}
	l->item = p;
	memcpy(p, ptr, size);
    if(list->current == NULL)
    {
        list->current = l;
        l->prev = NULL;
    }
    else
    {
		if(list->current->next != NULL)
		{
			list_end(list);
		}
        list->current->next = l;
        l->prev = list->current;
        list->current = l;
    }
	l->obj_size = size;
	l->next = NULL;
	list->size++;
	return 0;
}

bool list_has_next(List *list)
{
	ListItem *item = list->current;
	if(item->next == NULL || item == NULL)
	{
		return false;
	}
	return true;
}

bool list_has_prev(List *list)
{
	ListItem *item = list->current;
	if(item->prev == NULL || item == NULL)
	{
		return false;
	}
	return true;
}

bool list_next(List *list)
{
	ListItem *item = list->current;
    if(item == NULL)
        return false;
	if(item->next == NULL)
	{
		return false;
	}
	list->current = item->next;
	return true;
}

bool list_prev(List *list)
{
	ListItem *item = list->current;
    if(item == NULL)
        return false;
	if(item->prev == NULL)
	{
		return false;
	}
	list->current = item->prev;
	return true;
}

void *list_get(List *list)
{
    if(list->current != NULL)
        return list->current->item;
    else
        return NULL;
}

void *list_find(List *list, void* src, uint8_t size)
{
	uint8_t *obj;
	list_begin(list);
	do
	{
		obj = list_get(list);
		if(memcmp(obj, src, size) == 0)
			return obj;
	}while(list_next(list));
	return NULL;
}

void list_begin(List *list)
{
	while(list_prev(list));
}
void list_end(List *list)
{
	while(list_next(list));
}

void list_erase(List *list)
{
	//ListItem *li_current = list->current;
	ListItem *li_next = list->current->next;
	ListItem *li_prev = list->current->prev;
    if(li_prev != NULL)
        li_prev->next = li_next;
    if(li_next != NULL)
        li_next->prev = li_prev;
	free(list->current->item);
	free(list->current);
	if(li_next != NULL)
		list->current = li_next;
	else
		list->current = li_prev;
	list->size--;
}

void list_free(List *list)
{
    if(list->current == NULL)
        return;
	list_end(list);
	while(1)
	{
		ListItem *l = list->current;
		if(l == NULL)
		{
			break;
		}
		list->current = l->prev;
		free(l->item);
		free(l);
	}
}

bool list_iterator(ListIterator *it, List *list)
{
	//it->list = malloc(sizeof(List));
	//if(it->list == NULL)
	//	return false;
	memcpy(&it->list, list, sizeof(List));
	list_iterator_begin(it);
	it->current = NULL;
	return true;
}

//#define LIST_ITEM_HAS_NEXT(a) ()
bool list_item_has_next(ListItem *list_item)
{
	if(!list_item)
	{
		return true;	//DANGER!!!!
	}
	if(list_item->next)
	{
		return true;
	}
	return false;
}

bool list_item_has_prev(ListItem *list_item)
{
	if(!list_item)
	{
		return true;	//DANGER!!!!
	}
	if(list_item->prev)
	{
		return true;
	}
	return false;
}

bool list_iterator_next(ListIterator *it)
{		
	if(!list_has_next(&it->list))
	{
		it->current = NULL;
		return false;
	}
	
	if(it->current == NULL)
	{
		it->current = it->list.current;
		return true;
	}
	
    list_next(&it->list);
    it->current = it->list.current;
    
	return true;
}

bool list_iterator_prev(ListIterator *it)
{
	if(!list_has_prev(&it->list))
	{
		it->current = NULL;
		return false;
	}
	
	if(it->current == NULL)
	{
		it->current = it->list.current;
		return true;
	}
	
    list_prev(&it->list);
    it->current = it->list.current;
    
	return true;
}

void *list_iterator_find(ListIterator *it, void* src, uint8_t size)
{
	list_iterator_begin(it);
	while(list_iterator_next(it))
	{
		uint8_t *dst = list_iterator_get(it);
		if(it->current->obj_size != size)
			continue;
		if(memcmp(dst, src, size) == 0)
			return dst;
	}
	return NULL;
}