#include "ws2812.h"



void WS2812_send_zero_bit()
{
	WS_POTR_HIGH();
	asm("nop");asm("nop");
	WS_POTR_LOW();
        //asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");
       // asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");

}

void WS2812_send_one_bit()
{
	WS_POTR_HIGH();
	asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");
	asm("nop");asm("nop");asm("nop");asm("nop");
    asm("nop");asm("nop");asm("nop");
    WS_POTR_LOW();
        //asm("nop");asm("nop");asm("nop");asm("nop");
}

void WS2812_send(uint32_t data)
{
	for(uint8_t i = 0; i < 24; i++)
	{
		if(data & 0x800000)
		{
			WS2812_send_one_bit();
		}
		else
		{
			WS2812_send_zero_bit();
		}
		data <<= 1;
	}
        
}


void WS2812_send_color(struct WS2812_color *color)
{
    //uint32_t tmp = 
    WS2812_send(*((uint32_t*)color));
}

void WS2812_send_RGB(uint8_t red, uint8_t green, uint8_t blue)
{
    struct WS2812_color color;
    color.red = red;
    color.green = green;
    color.blue = blue / 5;
    WS2812_send_color(&color);
}

void WS2812_send_HSV(uint16_t hue, uint8_t saturation, uint8_t lightness)
{
	static uint8_t Vmin, Vinc, Vdec, V;
	static uint8_t* hsv_tb[6][3] = {
									{&V, &Vinc, &Vmin},
									{&Vdec, &V, &Vmin},
									{&Vmin, &V, &Vinc},
									{&Vmin, &Vdec, &V},
									{&Vinc, &Vmin, &V},
									{&V, &Vmin, &Vdec},
								 };
	
	V = lightness;
	
	uint8_t Hi = (hue / 60) % 6;
	Vmin = (100 - saturation) * V / 100;
	uint8_t a = (V - Vmin) * ((hue % 60) / 60);
	Vinc = Vmin + a;
	Vdec = V - a;
    
    Vmin = (uint8_t)(((uint16_t)Vmin * 255) / 100);
    Vinc = (uint8_t)(((uint16_t)Vinc * 255) / 100);
    Vdec = (uint8_t)(((uint16_t)Vdec * 255) / 100);
    V =    (uint8_t)(((uint16_t)V    * 255) / 100);
	
	WS2812_send_RGB(*hsv_tb[Hi][0], *hsv_tb[Hi][1], *hsv_tb[Hi][2]);
}

void WS2812_RGB_to_HSV(struct WS2812_color *color, uint16_t hue, uint8_t saturation, uint8_t lightness)
{
	static uint8_t Vmin, Vinc, Vdec, V;
	static uint8_t* hsv_tb[6][3] = {
									{&V, &Vinc, &Vmin},
									{&Vdec, &V, &Vmin},
									{&Vmin, &V, &Vinc},
									{&Vmin, &Vdec, &V},
									{&Vinc, &Vmin, &V},
									{&V, &Vmin, &Vdec},
								 };
	
	V = lightness;
	
	uint8_t Hi = (hue / 60) % 6;
	Vmin = (100 - saturation) * V / 100;
	uint8_t a = (V - Vmin) * ((hue % 60) / 60);
	Vinc = Vmin + a;
	Vdec = V - a;
    
    Vmin = (uint8_t)(((uint16_t)Vmin * 255) / 100);
    Vinc = (uint8_t)(((uint16_t)Vinc * 255) / 100);
    Vdec = (uint8_t)(((uint16_t)Vdec * 255) / 100);
    V =    (uint8_t)(((uint16_t)V    * 255) / 100);
	
	color->red = *hsv_tb[Hi][0];
	color->green = *hsv_tb[Hi][1];
	color->blue = *hsv_tb[Hi][2];
}


void WS2812_send_reset()
{
	WS_POTR_HIGH();
	asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");
	WS_POTR_LOW();
	uint8_t i;
	while(--i) asm("nop");
	WS_POTR_HIGH();
}