#ifndef WS2812_H
#define WS2812_H

#include "stm32f10x.h"

#define WS_POTR_HIGH() GPIO_SetBits(GPIOA, GPIO_Pin_2)
#define WS_POTR_LOW() GPIO_ResetBits(GPIOA, GPIO_Pin_2)

struct WS2812_color
{
	int8_t blue;
	int8_t red;
	int8_t green;
};

void WS2812_send_zero_bit();
void WS2812_send_one_bit();

void WS2812_send(uint32_t data);
void WS2812_send_color(struct WS2812_color *color);
void WS2812_send_RGB(uint8_t red, uint8_t green, uint8_t blue);
void WS2812_send_HSV(uint16_t hue, uint8_t saturation, uint8_t lightness);
void WS2812_send_reset();


void WS2812_RGB_to_HSV(struct WS2812_color *color, uint16_t hue, uint8_t saturation, uint8_t lightness);

#endif