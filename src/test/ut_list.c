#include "ut_list.h"

#include "list_legacy.h"
#include "unit_test.h"

#include "stdlib.h"
#include "string.h"
#include "stdint.h"


void ut_test_iterator_find()
{
	uint8_t *str_1 = "test_1";
	uint8_t *str_2 = "_test_2";
	uint8_t *str_3 = "_test__3";
	uint8_t *arr[] = {str_1, str_2, str_3};
	List list;
	ListIterator it;
	uint8_t *str_res;
	memset(&list, 0, sizeof(List));
	for(uint8_t i = 0; i < 3; i++)
	{
		list_push_back(&list, arr[i], strlen(arr[i]) + 1);
	}
	
	ut_begin();
	
	list_iterator(&it, &list);
	
	str_res = list_iterator_find(&it, "test_", strlen("test_"));
    assert_null(str_res);
	
	str_res = list_iterator_find(&it, "test_1", strlen("test_1") + 1);
    assert_not_null(str_res);
	assert_equals_str(str_res, "test_1");
	
	str_res = list_iterator_find(&it, "test_1", strlen("test_1"));
	assert_null(str_res);
	
	ut_end();
}

void ut_test_iterator_begin(void)
{
	uint8_t *str_1 = "test_1";
	uint8_t *str_2 = "_test_2";
	uint8_t *str_3 = "_test__3";
	uint8_t *arr[] = {str_1, str_2, str_3};
	List list;
	ListIterator it;
	uint8_t *str_res;
	memset(&list, 0, sizeof(List));
	for(uint8_t i = 0; i < 3; i++)
	{
		list_push_back(&list, arr[i], strlen(arr[i]) + 1);
	}
	
	
	ut_begin();
	
	list_iterator(&it, &list);
	
	list_iterator_next(&it);
	list_iterator_begin(&it);
    list_iterator_next(&it);
    str_res = list_iterator_get(&it);
	assert_equals_str(str_res, str_1);
    
	
	
	list_iterator_next(&it);
	list_iterator_begin(&it);
    list_iterator_next(&it);
    str_res = list_iterator_get(&it);
	assert_equals_str(str_res, str_1);
	
	
	list_iterator_next(&it);
	list_iterator_next(&it);
	list_iterator_begin(&it);
    list_iterator_next(&it);
    str_res = list_iterator_get(&it);
	assert_equals_str(str_res, str_1);
	
	ut_end();
}


void ut_list_test()
{
	ut_test_iterator_begin();
	ut_test_iterator_find();
}