#ifndef UNIT_TEST_H
#define UNIT_TEST_H

#include "stdbool.h"
#include "stdint.h"

#define DEBUG_LEVEL 0

#define assert_not_null(a) _assert_not_null(__FILE__, __LINE__, a)
#define assert_null(a) _assert_null(__FILE__, __LINE__, a)
#define assert_equals_u16(a, b) _assert_equals_u16(__FILE__, __LINE__, a, b)
#define assert_equals_str(a, b) _assert_equals_str(__FILE__, __LINE__, a, b)
#define assert_not_equals_str(a, b) _assert_not_equals_str(__FILE__, __LINE__, a, b)


void ut_error(void);
void ut_begin(void);
void ut_end(void);

void _ut_print_error(uint8_t *file, uint32_t line);

bool _assert_equals_str(uint8_t *file, uint32_t line, uint8_t *str1, uint8_t *str2);
bool _assert_not_equals_str(uint8_t *file, uint32_t line, uint8_t *str1, uint8_t *str2);
bool _assert_equals_u16(uint8_t *file, uint32_t line, uint16_t v1, uint16_t v2);
bool _assert_null(uint8_t *file, uint32_t line, void *v);
bool _assert_not_null(uint8_t *file, uint32_t line, void *v);

#endif