#include "test_allocation.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "sdcard.h"
#include "unit_test.h"

#include "list_legacy.h"
#include "directory.h"
//#include "playlist_v1.h"
//#include "playlist_m3u.h"
#include "playlist.h"

#include "setting.h"

/**/
#include "ut_playlist_tree.h"
#include "ut_directory.h"
#include "ut_list.h"

void test_list_allocation()
{
	//__iar_dlmalloc_stats();
	List list;
	memset(&list, 0, sizeof(List));
	scan_dir(&list, "0:/");
	printf("scandir\n");
	
	/*uint8_t str1[] = "test1";
	uint8_t str2[] = "test22";
	uint8_t str3[] = "test333";*/
	
	//printf("[list push back]\n");
	//list_push_back(&list, str1, sizeof(str1));
	//__iar_dlmalloc_stats();
	
	
	list_free(&list);
	printf("[list free]\n");
	//__iar_dlmalloc_stats();
}

void test_dir_allocation()
{
    Dir _dir;
	dir_path(&_dir, "0:/The Black Keys");
	printf("[dir load]\n");
	__iar_dlmalloc_stats();
	
    dir_next(&_dir);
	printf("[dir next]\n");
	__iar_dlmalloc_stats();
	
    dir_enter(&_dir);
	printf("[dir enter]\n");
	__iar_dlmalloc_stats();
    
    dir_enter(&_dir);
	printf("[dir enter]\n");
	__iar_dlmalloc_stats();
	
	dir_out(&_dir);
	printf("[dir out]\n");
	__iar_dlmalloc_stats();
	
	dir_free(&_dir);
	printf("[dir free]\n");
	__iar_dlmalloc_stats();
}

void test_playlist_allocation()
{
	printf("[init]\n");
	__iar_dlmalloc_stats();
    static Playlist *_playlist;
    uint8_t *path;
    
    Class_playlist* class_tree = playlist_singletion_factory(PE_TREE);
	_playlist = class_tree->create(class_tree);
    _playlist->this->load(_playlist, "0:/");
	_playlist->this->next(_playlist, &path);
    _playlist->this->free(_playlist);
    
    playlist_free(PE_TREE);

	__iar_dlmalloc_stats();
}

void test_playlist()
{

}

void test_play_list_m3u()
{
	/*m3u_load("0:/test.m3u");

	static uint8_t* sound_path;
	m3u_next(&sound_path);
	
	m3u_free();*/
	static uint8_t* sound_path;
    
    __iar_dlmalloc_stats();
    
	Class_playlist* class_m3u = playlist_singletion_factory(PE_M3U);
	Playlist *m3u = class_m3u->create(class_m3u);
    m3u->this->load(m3u, "0:/test.m3u");
    
    m3u->this->next(m3u, &sound_path);
    printf("%s \n", sound_path);
    m3u->this->next(m3u, &sound_path);
    printf("%s \n", sound_path);
    m3u->this->next(m3u, &sound_path);
    printf("%s \n", sound_path);
    
    m3u->this->free(m3u);
    playlist_free(PE_M3U);
	
    __iar_dlmalloc_stats();
}



void test_setting()
{
    __iar_dlmalloc_stats();
    Setting stg;
    static uint8_t *val;
    setting_create(&stg);
    setting_parse(&stg, "=play5");
    setting_parse(&stg, " ");
    setting_parse(&stg, "&f  d d");
    setting_parse(&stg, "playlist1=play1");
    setting_parse(&stg, "playlist2 = play2");
    setting_parse(&stg, "playlist3  =  play3");
    setting_parse(&stg, "playlist4  =");
    
    
    val = setting_get_key(&stg, "playlist1");
    assert_equals_str(val, "play1");
    val = setting_get_key(&stg, "playlist2");
    assert_equals_str(val, "play2");
    val = setting_get_key(&stg, "playlist3");
    assert_equals_str(val, "play3");
    val = setting_get_key(&stg, "playlist4");
    assert_null(val);
    setting_free(&stg);
    __iar_dlmalloc_stats();
}

void test_allocation()
{
	//test_dir_allocation();
	//test_playlist_allocation();
	//test_playlist();
	//test_play_list_m3u();
	//test_directory(); //test complite
	//ut_list_test(); //test complite
	test_play_list_tree();
    //test_setting();
}