#include "ut_playlist_tree.h"
#include "stdio.h"
#include "string.h"

#include "playlist.h"
#include "unit_test.h"
#include "playlist_v1.h"

#define UT_TREE_DEBUG_LEVEL 0

uint8_t unit_test_scan_dir(List *list, char *path)
{
	if(strcmp(path, "0:/") == 0)
	{
		uint8_t *buff[] = {"Metallica", "The black keys"};
		for(uint8_t i = 0; i < 2; i++)
		{
			list_push_back(list, buff[i], strlen((char const*)buff[i]) + 1);
		}
	}
	else if(strcmp(path, "0:/Metallica/") == 0)
	{
		uint8_t *buff[] = {"Kill Em All", "Ride The Lightning", "Master Of Puppets"};
		for(uint8_t i = 0; i < 3; i++)
		{
			list_push_back(list, buff[i], strlen((char const*)buff[i]) + 1);
		}
	}
	
	return 0;
}

/*****************************************unit tree test*/

void test_playlist_tree_prew()
{
	static uint8_t* sound_path;
    static Playlist_t playlist_status;
	
	ut_begin();
    
	Class_playlist* class_tree = playlist_singletion_factory(PE_TREE);
	Playlist *pltree = class_tree->create(class_tree);
    pltree->this->load(pltree, "0:/");
	
	playlist_status = pltree->this->next(pltree, &sound_path);
	#if UT_TREE_DEBUG_LEVEL == 1
	printf("[next][%i] %s\n", playlist_status, sound_path);
	#endif
	
	playlist_status = pltree->this->prev(pltree, &sound_path);
	#if UT_TREE_DEBUG_LEVEL == 1
	printf("[prew][%i] %s\n", playlist_status, sound_path);
	#endif
	
	playlist_status = pltree->this->prev(pltree, &sound_path);
	#if UT_TREE_DEBUG_LEVEL == 1
	printf("[prew][%i] %s\n", playlist_status, sound_path);
	#endif
	
	playlist_status = pltree->this->next(pltree, &sound_path);
	#if UT_TREE_DEBUG_LEVEL == 1
	printf("[next][%i] %s\n", playlist_status, sound_path);
	#endif
    assert_equals_u16(PL_OK, playlist_status);
	
    playlist_status = pltree->this->next(pltree, &sound_path);
    assert_equals_u16(PL_OK, playlist_status);
	
    pltree->this->free(pltree);
	
	ut_end();
}

void test_play_list_tree_next()
{
	uint16_t i;
	static uint8_t* sound_path;
	Playlist_t playlist_status;
	
	ut_begin();
	
	Class_playlist* class_tree = playlist_singletion_factory(PE_TREE);
	Playlist *pltree = class_tree->create(class_tree);
    pltree->this->load(pltree, "0:/");
	
	for(i = 0; i < 100; i++)
	{
		playlist_status = pltree->this->next(pltree, &sound_path);
		if(playlist_status == PL_END)
			break;
	}
	if(i == 100)
	{
		printf("TEST ERROR\n");
	}
	printf("%i\n", i);
    
    playlist_status = pltree->this->next(pltree, &sound_path);
	printf("[next][%i] %s\n", playlist_status, sound_path);
	
	playlist_status = pltree->this->prev(pltree, &sound_path);
	printf("[prew][%i] %s\n", playlist_status, sound_path);
	
	playlist_status = pltree->this->prev(pltree, &sound_path);
	printf("[prew][%i] %s\n", playlist_status, sound_path);
	
    pltree->this->free(pltree);
	
	ut_end();
}

void test_play_list_tree_common()
{
	static uint8_t* sound_path;
	
	ut_begin();
    
	Class_playlist* class_tree = playlist_singletion_factory(PE_TREE);
	Playlist *pltree = class_tree->create(class_tree);
    pltree->this->load(pltree, "0:/");
	
	for(uint8_t i = 0; i < 2; i++)
	{
		pltree->this->next(pltree, &sound_path);
		printf("[%i] %s\n", i, sound_path);
	}
	
	for(uint8_t i = 0; i < 1; i++)
	{
		pltree->this->prev(pltree, &sound_path);
		printf("[%i] %s\n", 4 - i, sound_path);
	}
    
    Playlist_t plres = pltree->this->prev(pltree, &sound_path);
    assert_equals_u16(PL_END, plres);
    
    pltree->this->prev(pltree, &sound_path);
    printf("[next] %s\n", sound_path);
	
    pltree->this->free(pltree);
	
	ut_end();
}

void test_play_list_special()
{
	Class_playlist* class_tree = playlist_singletion_factory(PE_TREE);
	Playlist *pltree = class_tree->create(class_tree);
	
    pltree->this->load(pltree, "0:/");
    
	
	printf("%s\n", pltree->path);
	for(uint8_t i = 0; i < 10; i++)
	{
		__playlist_tree_next_dir((Playlist_tree*)pltree);
		printf("%s\n", pltree->path);
	}
}

void test_playlist_to_dir()
{
	Dir_tree_state res_dir;
	List *list;
	Class_playlist* class_tree = playlist_singletion_factory(PE_TREE);
	Playlist_tree *pltree = class_tree->create(class_tree);
	
	ut_begin();
	
	res_dir = __playlist_to_dir(pltree, "0:/Metallica/");
	assert_equals_u16(res_dir, DR_OK);
    
    list_begin(&pltree->list_tree);
	list = list_get(&pltree->list_tree);
	assert_equals_str(list_get(list), "Metallica");
	list_next(&pltree->list_tree);
    list = list_get(&pltree->list_tree);
	assert_equals_str(list_get(list), "Kill Em All");
	assert_equals_u16(list_size(&pltree->list_tree), 2);
	
    
    res_dir = __playlist_to_dir(pltree, "0:/");
	assert_equals_u16(res_dir, DR_OK);
	
    list_begin(&pltree->list_tree);
	list = list_get(&pltree->list_tree);
	assert_equals_str(list_get(list), "Metallica");
	assert_equals_u16(list_size(&pltree->list_tree), 1);
	
	
	res_dir = __playlist_to_dir(pltree, "Metallica");
	assert_equals_u16(res_dir, DR_OK);
	
	list_begin(&pltree->list_tree);
	list = list_get(&pltree->list_tree);
	assert_equals_str(list_get(list), "Metallica");
	list_next(&pltree->list_tree);
    list = list_get(&pltree->list_tree);
	assert_equals_str(list_get(list), "Kill Em All");
	assert_equals_u16(list_size(&pltree->list_tree), 2);
	

	pltree->base.this->free(pltree);
	
	ut_end();
}

void test_play_list_tree()
{
	//__iar_dlmalloc_stats();
	
	//test_play_list_tree_next();
	//test_playlist_tree_prew();
	//test_play_list_tree_common();
	//test_play_list_special();
	test_playlist_to_dir();

	playlist_free(PE_TREE);	
	//__iar_dlmalloc_stats();
}