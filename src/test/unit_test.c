#include "unit_test.h"

#include "stdio.h"
#include "string.h"

uint16_t unit_test_count;
uint16_t assert_count;
uint8_t state_unit_test = 0;
uint32_t count_error = 0;

void _ut_error(void)
{
	count_error++;
}

void ut_begin(void)
{
	unit_test_count++;
	state_unit_test = 1;
	assert_count = 0;
	printf("[START]Unit test �%i\n", unit_test_count);
}

void ut_end(void)
{
	printf("[END] ");
	if(!state_unit_test)
	{
		printf("Unit test is not started\n");
		return;
	}
	if(count_error)
	{
		printf("success: %i, errors: %i \n", assert_count, count_error);
	}
	else
	{
		printf("Test[%i] success %i\n", unit_test_count, assert_count);
	}
	
	count_error = 0;
	state_unit_test = 0;
}

void _ut_print_error(uint8_t *file, uint32_t line)
{
#if DEBUG_LEVEL == 1
		printf("[ERROR] On line %i\n", line);
#elif DEBUG_LEVEL == 2
		printf("[ERROR] This fake error is in %s on line %i\n", file, line);
#endif
}

bool _assert_equals_str(uint8_t *file, uint32_t line, uint8_t *str1, uint8_t *str2)
{
	assert_count++;
	uint8_t len1 = strlen((char const*) str1);
	uint8_t len2 = strlen((char const*) str2);
	if(len1 != len2)
	{
		_ut_print_error(file, line);
		printf("[%s] = %i | ", str1, len1);
		printf("[%s] = %i\n", str2, len2);
		printf("Length lines does not equal %i != %i\n", len1, len2);
		_ut_error();
		return false;
	}
	
	if(strcmp((char const*) str1, (char const*) str2) != 0)
	{
		_ut_print_error(file, line);
		printf("The strings are not the same %s != %s\n", str1, str2);
		_ut_error();
		return false;
	}
	return true;
}

bool _assert_not_equals_str(uint8_t *file, uint32_t line, uint8_t *str1, uint8_t *str2)
{
	assert_count++;
	uint8_t len1 = strlen((char const*) str1);
	uint8_t len2 = strlen((char const*) str2);
	if(len1 != len2)
	{
		return true;
	}
	
	if(strcmp((char const*) str1, (char const*) str2) != 0)
	{
		return true;
	}
	
	_ut_print_error(file, line);
	printf("These lines are the same %s != %s\n", str1, str2);
	_ut_error();
	return false;
}

bool _assert_equals_u16(uint8_t *file, uint32_t line, uint16_t v1, uint16_t v2)
{
	assert_count++;
    if(v1 != v2)
    {
		_ut_print_error(file, line);
        printf("Value in not equals %i != %i\n", v1, v2);
		_ut_error();
        return false;
    }
    return true;
}

bool _assert_null(uint8_t *file, uint32_t line, void *v)
{
	assert_count++;
	if(v != NULL)
	{
		_ut_print_error(file, line);
		printf("Value is not NULL (0x%X)\n", v);
		_ut_error();
		return false;
	}
	return true;
}

bool _assert_not_null(uint8_t *file, uint32_t line, void *v)
{
	assert_count++;
	if(v == NULL)
	{
		_ut_print_error(file, line);
		printf("Value is NULL (0x%X)\n", v);
		_ut_error();
		return false;
	}
	return true;
}