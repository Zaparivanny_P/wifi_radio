#include "ut_directory.h"

#include "directory.h"
#include "unit_test.h"

void test_dir_get()
{
	uint8_t *test_str_0 = "0:/test/";
	uint8_t *test_str_1 = "0:/test3";
	uint8_t *test_str_2 = "";
	uint8_t *test_str_3 = "0:/";
	uint8_t *test_str[] = {test_str_0,
							test_str_1,
							test_str_2,
							test_str_3};
	uint8_t ans[4][2] = {{3, 4},
						{3, 5},
						{3, 0},
						{3, 0}};
	ut_begin();
	for(uint8_t i = 0; i < 4; i++)
	{
		uint8_t res = _dir_get(test_str[i], ans[i][0]);
		assert_equals_u16(res, ans[i][1]);
	}
	ut_end();
}


void test_directory()
{
	test_dir_get();
}