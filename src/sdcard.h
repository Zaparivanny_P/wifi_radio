#ifndef SDCARD_H
#define SDCARD_H

#include "stm32f10x.h"
#include "sdio_sd.h"
#include "ff.h"
#include "diskio.h"

#include "list_legacy.h"

#define SDIO_FIFO_ADDRESS                ((uint32_t)0x40018080)

FRESULT scan_dir(List *list, char *path);
FRESULT scan_files(List *list, char *path);
void sdcard_init();
#endif