#include "playlist_v1.h"

#include "directory.h"
#include "sdcard.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"

Class_playlist_tree* tree_create_class()
{
	Class_playlist_tree *ptr = malloc(sizeof(Class_playlist_tree));
	if(ptr == NULL)
		return NULL;
	ptr->base.create = playlist_tree_create;
	ptr->base.load = playlist_tree_load;
	ptr->base.restore = playlist_tree_restore;
	ptr->base.next = playlist_tree_next;
	ptr->base.prev = playlist_tree_prev;
	ptr->base.free = playlist_tree_free;
	ptr->setting = playlist_tree_setting;
    return ptr;
}

void pl_tree_free_class(Class_playlist_tree* pl_tree_class)
{
	free(pl_tree_class);
}

void* playlist_tree_create(struct Class_playlist* cpl)
{
	Playlist_tree* p = malloc(sizeof(Playlist_tree));
	if(p == NULL)
		return NULL;
	((Playlist*)p)->this = (Class_playlist*)cpl;
	p->base.path = malloc(sizeof(uint8_t) * 256);
	if(p->base.path == NULL)
	{
		free(p);
        return NULL;
	}
	((Playlist*)p)->type = PE_TREE;
	memset(&p->sound_list, 0, sizeof(List));
	memset(&p->list_tree, 0, sizeof(List));
	return p;
}

Playlist_t playlist_tree_setting(void* obj, uint16_t flags)
{
	Playlist_tree* pl = obj;
	pl->flags = flags;
    return PL_OK;
}

Playlist_t playlist_tree_load(void* obj, uint8_t* file_path)	//todo free list
{
	Playlist_tree* pl = obj;
    
	memset(pl->base.path, 0, sizeof(uint8_t) * 256);
	strcpy((char*)pl->base.path, (const char*)file_path);
	
    playlist_v2_find(&pl->list_tree, pl->base.path);
    
	FRESULT res = scan_files(&pl->sound_list, (char*)pl->base.path);
    memset(&pl->pl_it, 0, sizeof(ListIterator));
	list_iterator(&pl->pl_it, &pl->sound_list);
    return PL_OK;
}

Playlist_t playlist_tree_restore(void* obj, uint8_t* file_path)
{
	return PL_OK;
}

void playlist_load_list_dir(void* obj, char *path)
{
    FRESULT res;
	List _list;
	Playlist_tree* pl = obj;
	memset(&_list, 0, sizeof(List));
#if UNIT_TEST_SCAN_DIR == 1
	res = unit_test_scan_dir(&_list, path);
#else
	res = scan_dir(&_list, path);
#endif
	if(list_size(&_list) != 0)
	{
		list_begin(&_list);
		list_push_back(&pl->list_tree, &_list, sizeof(List));
	}
}

Dir_tree_state __playlist_to_dir(void* obj, uint8_t *path)
{
	uint8_t i, offset = 0;

	Playlist_tree* pl = obj;
	
	if(path[1] == ':' && path[2] == '/')
	{
        memset(pl->base.path, 0, sizeof(uint8_t) * 256);
		playlist_tree_free_list(pl);
	}
	else
	{
		offset = strlen(pl->base.path);
	}
	
	for(i = 0; i < strlen(path); i++)
	{
		if(path[i] == '/')
		{
			memcpy(pl->base.path + offset, path, i + 1);
			playlist_load_list_dir(pl, pl->base.path);
		}
	}
	
	if(i == strlen(path) && path[i - 1] != '/')
	{
		memcpy(pl->base.path + offset, path, i + 1);
		pl->base.path[strlen(pl->base.path)] = '/';
		playlist_load_list_dir(pl, pl->base.path);
	}
	
	return DR_OK;
}

Dir_tree_state __playlist_tree_next_dir(void* obj)
{
	uint8_t *dir;
	Playlist_tree* pl = obj;
	if(pl->dr_state == DR_END)
		return DR_END;
	
	List *_list = list_get(&pl->list_tree);
	if(_list == NULL)
	{
		pl->dr_state = DR_END;
		return DR_END;
	}
	
	if(!list_next(_list))
	{
		if(list_size(&pl->list_tree) == 1)
		{
			pl->dr_state = DR_END;
			return DR_END;
		}
		else
		{
			list_free(_list);
			list_erase(&pl->list_tree);
			_dir_out(pl->base.path);
			return DR_OK;
		}
	}
	
	_dir_out(pl->base.path);
	_list = list_get(&pl->list_tree);
	dir = list_get(_list);
	_dir_enter(pl->base.path, dir);
	playlist_v2_find(&pl->list_tree, pl->base.path);
    return DR_OK;
}

Playlist_t playlist_tree_next_dir(void* obj, uint8_t** sound_path)
{
	Playlist_tree* pl = obj;
	List *_list = list_get(&pl->list_tree);
    uint8_t *dir;
	if(!list_next(_list))
	{
        
		if(list_size(&pl->list_tree) == 1)
		{
			return PL_END;
		}
		
		list_free(_list);
		list_erase(&pl->list_tree);
		_dir_out(*sound_path);
		return PL_OK;
	}
	
	_dir_out(*sound_path);
	_list = list_get(&pl->list_tree);
	dir = list_get(_list);
	_dir_enter(*sound_path, dir);
	playlist_v2_find(&pl->list_tree, *sound_path);
    return PL_OK;
}

Playlist_t playlist_tree_prev_dir(void* obj, uint8_t** sound_path)
{
	Playlist_tree* pl = obj;
	List *_list = list_get(&pl->list_tree);	// get sequence of nested lists folders
    uint8_t *dir;
		
	List _list_dir;
	memset(&_list_dir, 0, sizeof(List));
	FRESULT res = scan_dir(&_list_dir, (char*)*sound_path);	//todo handler result
	if(list_size(&_list_dir) != 0)
	{
		list_end(&_list_dir);
		_dir_enter(*sound_path, list_get(&_list_dir));
		list_push_back(&pl->list_tree, &_list_dir, sizeof(List));
	}
	else
	{
        if(_list == NULL)
            return PL_INTERNAL_ERROR;
		if(!list_prev(_list))
		{
			if(list_size(&pl->list_tree) == 1)
			{
				return PL_END;
			}
			else	
			{
				list_free(_list);
				list_erase(&pl->list_tree);
			}			
		}
        _dir_out(*sound_path);
        _list = list_get(&pl->list_tree);
        dir = list_get(_list);
        _dir_enter(*sound_path, dir);
	}
	
    return PL_OK;
}

Playlist_t playlist_tree_find_next(Playlist_tree* pl)
{
	Playlist_t plres = __playlist_tree_next_dir(pl);
	if(plres != PL_OK)
    {
        return plres;
    }
	
    list_free(&pl->sound_list);
    memset(&pl->sound_list, 0, sizeof(List));
	FRESULT res = scan_files(&pl->sound_list, (char*)pl->base.path);
	list_iterator(&pl->pl_it, &pl->sound_list);
	return PL_OK;
}

Playlist_t playlist_tree_find_prev(Playlist_tree* pl)
{
    Playlist_t plres = playlist_tree_prev_dir(pl, &pl->base.path);
	if(plres != PL_OK)
    {
        return plres;
    }
    list_free(&pl->sound_list);
    memset(&pl->sound_list, 0, sizeof(List));
	FRESULT res = scan_files(&pl->sound_list, (char*)pl->base.path);
	list_iterator(&pl->pl_it, &pl->sound_list);
    list_iterator_end(&pl->pl_it);
    return PL_OK;
}

Playlist_t playlist_tree_next(void* obj, uint8_t** sound_path)
{
	Playlist_tree* pl = obj;
	if(list_iterator_next(&pl->pl_it))
	{
		*sound_path = (uint8_t*)list_iterator_get(&pl->pl_it);
		return PL_OK;
	}
	else
	{
		Playlist_t plres = playlist_tree_find_next(obj);
		if(plres != PL_OK)
			return plres;
		return playlist_tree_next(obj, sound_path);
	}
}

Playlist_t playlist_tree_prev(void* obj, uint8_t** sound_path)
{
    Playlist_tree* pl = obj;
	if(list_iterator_prev(&pl->pl_it))
	{
		*sound_path = (uint8_t*)list_iterator_get(&pl->pl_it);
		return PL_OK;
	}
	else
	{
		Playlist_t plres = playlist_tree_find_prev(obj);
		if(plres != PL_OK)
			return plres;
		return playlist_tree_prev(obj, sound_path);
	}
	//return PL_OK;
}

void playlist_tree_free_list(Playlist_tree *pl)
{
	list_begin(&pl->list_tree);
	do
	{
		List *_list = list_get(&pl->list_tree);
        if(_list == NULL)
            continue;
		list_free(_list);        
	}while(list_next(&pl->list_tree));
	memset(&pl->list_tree, 0, sizeof(List));
}

void playlist_tree_free(void* obj)
{
	Playlist_tree* pl = obj;
	list_free(&pl->sound_list);
	playlist_tree_free_list(pl);
	list_free(&pl->list_tree);
	free(pl->base.path);
	free(pl);
}

void playlist_v2_find(List *_tree_list, uint8_t *path)
{
	while(1)
	{
		List _list;
		memset(&_list, 0, sizeof(List));
		FRESULT res = scan_dir(&_list, (char*)path);
		if(list_size(&_list) != 0)
		{
			list_begin(&_list);
			_dir_enter(path, list_get(&_list));
			list_push_back(_tree_list, &_list, sizeof(List));
		}
		else
		{
			break;
		}
	}
	list_end(_tree_list);
}
