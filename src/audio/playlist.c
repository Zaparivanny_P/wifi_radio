#include "playlist.h"
#include "stddef.h"
#include "stdlib.h"

#include "playlist_m3u.h"
#include "playlist_v1.h"

static Class_playlist_m3u*  _playlist_m3u;
static Class_playlist_tree* _playlist_tree;

Class_playlist* playlist_singletion_factory(PlayEngine play_type)
{
	switch(play_type)
	{
		case PE_M3U:
            if(_playlist_m3u == NULL)
                _playlist_m3u = m3u_create_class();
            return (Class_playlist*)_playlist_m3u;
			
		case PE_TREE:
            if(_playlist_tree == NULL)
                _playlist_tree = tree_create_class();
            return (Class_playlist*)_playlist_tree;
		
		default:
			break;
	}
    return NULL;
}


void playlist_free(PlayEngine play_type)
{
    switch(play_type )
    {
    case PE_M3U:
        if(_playlist_m3u != NULL)
        {
            free(_playlist_m3u);
            _playlist_m3u = NULL;
        }
        break;
    case PE_TREE:
        if(_playlist_tree != NULL)
        {
            free(_playlist_tree);
            _playlist_tree = NULL;
        }
        break;
    }
}