#ifndef PLAYLIST_M3U_H
#define PLAYLIST_M3U_H

#include "playlist.h"


typedef struct Class_playlist_m3u
{
	Class_playlist base;
}Class_playlist_m3u;

typedef struct
{
	Playlist pl;
}Playlist_m3u;


Class_playlist_m3u* m3u_create_class();
void m3u_free_class(Class_playlist_m3u* m3u_class);


void* m3u_create(struct Class_playlist* cpl);

Playlist_t m3u_load(void* obj, uint8_t* file_path);
Playlist_t m3u_restore(void* obj, uint8_t* file_path);
Playlist_t m3u_next(void* obj, uint8_t** sound_path);
Playlist_t m3u_prev(void* obj, uint8_t** sound_path);

void m3u_free(void* obj);


#endif