#ifndef DIRECTORY_H
#define DIRECTORY_H
#include "stdint.h"
#include "list_legacy.h"

void _dir_enter(uint8_t *path, uint8_t *dir);
void _dir_out(uint8_t *path);
uint8_t _dir_get(uint8_t *path, uint8_t offset);	// input - path and offset, return size name; example path = 0:/test/input, index = 3, return 4

typedef struct 
{
	uint8_t *path;
	List list;
	ListIterator it;
}Dir;

uint8_t dir_path(Dir *dir, uint8_t *path);
bool dir_next(Dir *dir);
bool dir_prev(Dir *dir);
bool dir_enter(Dir *dir);
bool dir_out(Dir *dir);
void dir_free(Dir *dir);

static inline uint8_t* dir_current_path(Dir *dir)
{
	//return list_iterator_get(&dir->it);
    return dir->path;
}

static inline uint8_t* dir_current_dir(Dir *dir)
{
	return list_iterator_get(&dir->it);
}


#endif