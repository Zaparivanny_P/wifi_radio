#include "directory.h"
#include "string.h"
#include "stdlib.h"
#include "sdcard.h"

void _dir_enter(uint8_t *path, uint8_t *dir)
{
	if(path[strlen((const char*)path) - 1] !=  '/')
		strcat((char*)path, "/");
	strcat((char*)path, (const char *)dir);
}

void _dir_out(uint8_t *path)
{
	uint8_t n = strlen((const char*)path);
	for(uint8_t i = 0; i < n; i++)
	{
		if(path[n - i] == ':')
		{
			break;
		}
		if(path[n - i] == '/')
		{
			path[n - i] = 0;
			break;
		}
        path[n - i] = 0;
	}
}

uint8_t _dir_get(uint8_t *path, uint8_t offset)
{
	uint8_t i = 0;
	if(strlen(path) < offset)
		return 0;
	for(; i < strlen(path) - offset; ++i)
	{
		if(path[i + offset] == '/')
			return i;
	}
	return i;
}


uint8_t dir_path(Dir *dir, uint8_t *path)
{
	uint8_t size = strlen((const char*)path) + 1;
	dir->path = malloc(size * sizeof(uint8_t));
    if(dir->path == NULL)
        return -1;
	strcpy((char*)dir->path, (const char*)path);
	memset(&dir->list, 0, sizeof(List));
	FRESULT res = scan_dir(&dir->list, (char*)dir->path);
	if(res != FR_OK)
		return -1;
	bool r = list_iterator(&dir->it, &dir->list);
	if(!r)
		return -1;
    
    dir_next(dir);
    return 0;
}


bool dir_next(Dir *dir)
{
	if(list_iterator_next(&dir->it))
	{
		return true;
	}
	return false;
}

bool dir_prev(Dir *dir)
{
	if(list_iterator_prev(&dir->it))
	{
		return true;
	}
	return false;
}

bool dir_enter(Dir *dir)
{
	uint8_t* _dir = list_iterator_get(&dir->it);
    if(_dir == NULL)
        return false;
	uint8_t  size = strlen((const char*)dir->path) + strlen((const char*)_dir) + 1;
	uint8_t* sz = realloc(dir->path, size * sizeof(uint8_t));
	if(sz == NULL)
	{
		return false;
	}
    dir->path = sz;
	_dir_enter(dir->path, _dir);
	list_free(&dir->list);
	memset(&dir->list, 0, sizeof(List));
	
	FRESULT res = scan_dir(&dir->list, (char*)dir->path);
	if(res != FR_OK)
		return false;
	bool r = list_iterator(&dir->it, &dir->list);
	if(!r)
		return false;
	return true;
}

bool dir_out(Dir *dir)
{
	_dir_out(dir->path);
	uint8_t size = strlen((const char*)dir->path) + 1;
	uint8_t* sz = realloc(dir->path, size * sizeof(uint8_t));
	if(sz == NULL)
	{
		return false;
	}
    dir->path = sz;
	list_free(&dir->list);
	memset(&dir->list, 0, sizeof(List));
	
	FRESULT res = scan_dir(&dir->list, (char*)dir->path);
	if(res != FR_OK)
		return false;
	bool r = list_iterator(&dir->it, &dir->list);
	if(!r)
		return false;
	return true;
}

void dir_free(Dir *dir)
{
	list_free(&dir->list);
	free(dir->path);
	dir = NULL;
}