#ifndef PLAYLIST_H
#define PLAYLIST_H

#include "stm32f10x.h"
#include "playlist.h"
#include "list_legacy.h"

#define PL_DIR_REPEAT 0x1
#define PL_DIR_ONLY   0x2
#define PL_SND_REPEAT 0x4

void playlist_v1_load(uint8_t *path);
uint8_t *playlist_v1_next();
uint8_t *playlist_v1_prew();

typedef enum
{
	DR_OK = 0,
	DR_BEGIN,
	DR_END,
	DR_ERROR_PATH,
}Dir_tree_state;


typedef struct Class_playlist_tree
{
	Class_playlist base;
	Playlist_t (*setting)(void* obj, uint16_t flags);
}Class_playlist_tree;

typedef struct
{
	Playlist base;
	ListIterator pl_it;
	List list_tree;
	List sound_list;
	uint16_t flags;
	Dir_tree_state dr_state;
}Playlist_tree;


Class_playlist_tree* tree_create_class();
void pl_tree_free_class(Class_playlist_tree* pl_tree_class);


void* playlist_tree_create(struct Class_playlist* cpl);
Playlist_t playlist_tree_setting(void* obj, uint16_t flags);
Playlist_t playlist_tree_load(void* obj, uint8_t* file_path);
Playlist_t playlist_tree_restore(void* obj, uint8_t* file_path);
Playlist_t playlist_tree_next(void* obj, uint8_t** sound_path);
Playlist_t playlist_tree_prev(void* obj, uint8_t** sound_path);
void playlist_tree_free(void* obj);

void playlist_tree_free_list(Playlist_tree *pl);
void playlist_v2_find(List *_tree_list, uint8_t *path);

void playlist_load_list_dir(void* obj, char *path);

Dir_tree_state __playlist_tree_next_dir(void* obj);
Dir_tree_state __playlist_to_dir(void* obj, uint8_t *path);

#if UNIT_TEST_SCAN_DIR == 1
uint8_t unit_test_scan_dir(List *list, char *path);
#endif

#endif