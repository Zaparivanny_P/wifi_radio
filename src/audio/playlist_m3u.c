#include "playlist_m3u.h"
#include "stddef.h"
#include "string.h"
#include "stdlib.h"

#include "sdcard.h"

FIL m3u_fil;

Class_playlist_m3u* m3u_create_class()
{
	Class_playlist_m3u *ptr = malloc(sizeof(Class_playlist_m3u));
	if(ptr == NULL)
		return NULL;
	ptr->base.create = m3u_create;
	ptr->base.load = m3u_load;
	ptr->base.restore = m3u_restore;
	ptr->base.next = m3u_next;
	ptr->base.prev = m3u_prev;
	ptr->base.free = m3u_free;
    return ptr;
}

void m3u_free_class(Class_playlist_m3u* m3u_class)
{
	free(m3u_class);
}

void* m3u_create(struct Class_playlist* cpl)
{
	Playlist_m3u* p = malloc(sizeof(Playlist_m3u));
	if(p == NULL)
		return NULL;
	((Playlist*)p)->this = (Class_playlist*)cpl;
	((Playlist*)p)->type = PE_M3U;
    ((Playlist*)p)->path = malloc(sizeof(uint8_t) * 256);
	if(((Playlist*)p)->path == NULL)
		return NULL;
	return p;
}

Playlist_t m3u_load(void* obj, uint8_t* file_path)
{
	FRESULT fr = f_open(&m3u_fil, (char const*)file_path, FA_READ);
	if(fr)
		return PL_FL_NO_FOUND;
	return PL_OK;
}

Playlist_t m3u_restore(void* obj, uint8_t* file_path)
{
	return PL_INTERNAL_ERROR;
}

Playlist_t m3u_next(void* obj, uint8_t** sound_path)
{
	
	TCHAR* p;
	uint8_t *buffer = ((Playlist*)p)->path;
	while(1)
	{
		p = f_gets((char*)buffer, 256, &m3u_fil);
		if(p == NULL)
			return PL_END;
		if((p[0] == '#') || (p[0] == ' ') || (strlen(p) <= 1))
			continue;
		if((p[1] == ':') && (p[2] == '\\'))
		{
            p[0] = '0';
			*sound_path = (uint8_t*)p;
			return PL_OK;
		}
	}
}


Playlist_t m3u_prev(void* obj, uint8_t** sound_path)
{
	return PL_OK;
}

void m3u_free(void* obj)
{
	free(((Playlist*)obj)->path);
	f_close(&m3u_fil);
	free(obj);
}