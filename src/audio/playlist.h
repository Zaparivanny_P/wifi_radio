#ifndef _PLAYLIST_H
#define _PLAYLIST_H

#include "stdint.h"

#define PLAYLISTS 2

typedef enum
{
	PL_OK = 0x0,
	PL_FL_NO_FOUND,
	PL_END,
	PL_SIG_FAULT,
    PL_INTERNAL_ERROR,
}Playlist_t;


typedef enum
{
	PE_M3U = 0x1,
	PE_TREE = 0x2,
}PlayEngine;

struct Class_playlist;

typedef struct
{
	uint8_t* path;
	struct Class_playlist* this;
	PlayEngine type;
}Playlist;

typedef struct Class_playlist
{
	void* (*create)(struct Class_playlist* cpl);
	Playlist_t (*load)(void* obj, uint8_t* file_path);
	Playlist_t (*restore)(void* obj, uint8_t* file_path);
	Playlist_t (*next)(void* obj, uint8_t** sound_path);
	Playlist_t (*prev)(void* obj, uint8_t** sound_path);
	void (*free)(void* obj);
}Class_playlist;

Class_playlist* playlist_singletion_factory(PlayEngine play_type);
void playlist_free(PlayEngine play_type);

#endif