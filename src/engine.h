#ifndef ENGINE_H
#define ENGINE_H

#include "stm32f10x.h"

#define BTN_PREW 0
#define BTN_ENTR 1
#define BTN_NEXT 2

uint8_t* engine_playlist_v1_next();
void engine_playlist_v1_load(uint8_t *path);

/*public function*/
void engine_init();
void engine_update();
void engine_next_audio_file();
void engine_prev_audio_file();
void engine_pause();
void engine_resume();
void engine_stream_audio();


void engine_low_power(void);
void engine_high_power(void);
void engine_reinit_to_low_power(void);
void engine_reinit_to_high_power(void);

#endif