#ifndef VS1003_H
#define VS1003_H

#include "stm32f10x.h"
#include "board.h"

#define VS1003_XRESET_HIGH() GPIO_ResetBits(_PORT(PORT_XRESET), _PIN(PIN_XRESET))
#define VS1003_XRESET_LOW()  GPIO_SetBits(_PORT(PORT_XRESET), _PIN(PIN_XRESET))

#define VS1003_XDCS_HIGH() GPIO_ResetBits(_PORT(PORT_XDCS), _PIN(PIN_XDCS)); //_delay(20);
#define VS1003_XDCS_LOW() GPIO_SetBits(_PORT(PORT_XDCS), _PIN(PIN_XDCS))	

#define VS1003_DREQ_HIGH() GPIO_SetBits(_PORT(PORT_DREQ), _PIN(PIN_DREQ))	
#define VS1003_DREQ_LOW() GPIO_ResetBits(_PORT(PORT_DREQ), _PIN(PIN_DREQ))	

#define VS1003_CS_HIGH() GPIO_ResetBits(_PORT(PORT_CS), _PIN(PIN_CS))	
#define VS1003_CS_LOW() GPIO_SetBits(_PORT(PORT_CS), _PIN(PIN_CS))	


#define VS_WRITE_COMMAND        0x02
#define VS_READ_COMMAND         0x03
#define SPI_MODE                0x00
#define SPI_STATUS              0x01
#define SPI_BASS                0x02
#define SPI_CLOCKF              0x03
#define SPI_DECODE_TIME         0x04
#define SPI_AUDATA              0x05
#define SPI_WRAM                0x06
#define SPI_WRAMADDR            0x07
#define SPI_HDAT0               0x08
#define SPI_HDAT1               0x09
#define SPI_AIADDR              0x0a
#define SPI_VOL                 0x0b
#define SPI_AICTRL0             0x0c
#define SPI_AICTRL1             0x0d
#define SPI_AICTRL2             0x0e
#define SPI_AICTRL3             0x0f
#define SM_DIFF                 0x01
#define SM_JUMP                 0x02
#define SM_RESET                0x04
#define SM_OUTOFWAV             0x08
#define SM_PDOWN                0x10
#define SM_TESTS                0x20
#define SM_STREAM               0x40
#define SM_PLUSV                0x80
#define SM_DACT                 0x100
#define SM_SDIORD               0x200
#define SM_SDISHARE             0x400
#define SM_SDINEW               0x800
#define SM_ADPCM                0x1000
#define SM_ADPCM_HP             0x2000


void vs1003_sine_test();
void vs1003_start();

uint8_t vs1003_send(uint8_t data);
void vs1003_write_reg(uint8_t address, uint8_t hi, uint8_t lo);
uint16_t vs1003_read_reg(uint8_t addressbyte);

void vs1003_set_treble(uint8_t xOneAndHalfdB);
uint8_t vs1003_get_treble();

void vs1003_set_volume(uint8_t volume);
uint8_t vs1003_get_volume();
void _delay(uint32_t ms);


#endif