#include "vs1003.h"


void _delay(uint32_t ms)
{
	unsigned int i;
	unsigned long j;
	for(i = ms; i > 0; i--)
		for(j = 1000; j > 0; j--);
}

uint8_t vs1003_send(uint8_t data)
{
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(SPI1, data);
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
	return SPI_I2S_ReceiveData(SPI1);
}

void vs1003_write_reg(uint8_t address, uint8_t hi, uint8_t lo)
{
    static uint8_t req = 0;
	VS1003_XDCS_LOW();
	while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);
	VS1003_CS_HIGH();
	req = vs1003_send(VS_WRITE_COMMAND);
	req = vs1003_send(address);
	req = vs1003_send(hi);
	req = vs1003_send(lo);
    while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);
	VS1003_CS_LOW();
}

uint16_t vs1003_read_reg(uint8_t addressbyte)
{
    uint16_t result;
    VS1003_XDCS_LOW();
    while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);
    VS1003_CS_HIGH();
    vs1003_send(VS_READ_COMMAND);
    vs1003_send(addressbyte);
    result = vs1003_send(0) << 8;
    result |= vs1003_send(0);
    while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);
    VS1003_CS_LOW();
    return result;
}


void vs1003_soft_reset()
{
	vs1003_write_reg(SPI_MODE, 0x00, 0x04);
}


void vs1003_reset_chip()
{
	VS1003_XRESET_HIGH();
	_delay(100);
	vs1003_send(0xFF);
	VS1003_CS_LOW();
	VS1003_XDCS_LOW();
	VS1003_XRESET_LOW();
	vs1003_soft_reset();
	_delay(100);

	while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);
	_delay(100);
}
/*
void vs1003_start()
{
	vs1003_reset_chip();
	
	
	
}*/

void vs1003_start()
{
	VS1003_XRESET_HIGH();
	_delay(1000);
	vs1003_send(0xFF);
	VS1003_CS_LOW();
	VS1003_XDCS_LOW();
	VS1003_XRESET_LOW();
	vs1003_soft_reset();
	_delay(1000);

	while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);

	vs1003_write_reg(SPI_MODE, 0x08, 0x00);
	vs1003_write_reg(SPI_CLOCKF, 0x9B, 0xE8);
	vs1003_write_reg(SPI_AUDATA, 0xAC, 0x45);
    //vs1003_write_reg(SPI_AUDATA, 0x2B, 0x10);
	vs1003_write_reg(SPI_BASS, 0x00, 0xf6);
	vs1003_write_reg(SPI_VOL, 0x30, 0x30);
    //vs1003_write_reg(SPI_BASS, 0x08, 0x00);
    //vs1003_write_reg(SPI_VOL, 0x00, 0x00);
	vs1003_write_reg(SPI_STATUS, 0, 0x33);//0b00110011);
	
    static uint8_t data;
    data = vs1003_read_reg(SPI_DECODE_TIME);
    data = vs1003_read_reg(SPI_STATUS);
    data = vs1003_read_reg(SPI_CLOCKF);
	//vs1003_write_reg(SPI_DECODE_TIME, 0, 0);
	//vs1003_write_reg(SPI_DECODE_TIME, 0, 0);
	

	while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);
}


void vs1003_sine_test()
{
	VS1003_XRESET_HIGH();
	vs1003_reset_chip();
	_delay(1000);
	vs1003_send(0xff);

	VS1003_CS_LOW();
	VS1003_XDCS_LOW();
	VS1003_XRESET_LOW();

	_delay(500);
	vs1003_write_reg(SPI_MODE, 0x08, 0x20);
	_delay(500);

	while(GPIO_ReadInputDataBit(_PORT(PORT_DREQ), _PIN(PIN_DREQ)) == 0);

	VS1003_XDCS_HIGH();
	vs1003_send(0x53);
	vs1003_send(0xef);
	vs1003_send(0x6e);
	vs1003_send(0x24);
	vs1003_send(0x00);
	vs1003_send(0x00);
	vs1003_send(0x00);
	vs1003_send(0x00);
	_delay(100);
	VS1003_XDCS_LOW();
	
	
	VS1003_XDCS_HIGH();
	vs1003_send(0x45);
	vs1003_send(0x78);
	vs1003_send(0x69);
	vs1003_send(0x74);
	vs1003_send(0x00);
	vs1003_send(0x00);
	vs1003_send(0x00);
	vs1003_send(0x00);
	_delay(100);
	VS1003_XDCS_LOW();

}

uint8_t vs1003_get_treble()
{
    static uint8_t req;
    req = vs1003_read_reg(SPI_BASS);
	return ((req & 0xF000) >> 12);
}

uint16_t MaskAndShiftRight(uint16_t Source, uint16_t Mask, uint16_t Shift)
{
        return ( (Source & Mask) >> Shift );
}

void vs1003_set_treble(uint8_t xOneAndHalfdB)
{
	uint16_t bassReg = vs1003_read_reg(SPI_BASS);
	if (xOneAndHalfdB <= 8)
		vs1003_write_reg(SPI_BASS, MaskAndShiftRight(bassReg, 0x0F00, 8) | (xOneAndHalfdB << 4), bassReg & 0x00FF);
	else
		vs1003_write_reg(SPI_BASS, MaskAndShiftRight(bassReg, 0x0F00, 8) | 0x80, bassReg & 0x00FF);
}


void vs1003_set_volume(uint8_t volume)
{
	vs1003_write_reg(SPI_VOL, volume, volume);
}

uint8_t vs1003_get_volume()
{
	return (vs1003_read_reg(SPI_VOL) & 0x00FF);
}


uint16_t vs1003_get_decode_time()
{
	return vs1003_read_reg(SPI_DECODE_TIME);
}
