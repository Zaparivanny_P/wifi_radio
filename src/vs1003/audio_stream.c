#include "audio_stream.h"
#include "string.h"
#include "stdio.h"

#include "ring_buffer.h"

static  Ring_buffer rbuff;
static uint8_t rarr[1024 * 13];
static uint32_t w_cnt, w_length;
static uint32_t cnt_frame = 0;

uint8_t audio_stream_init(void)
{
	rign_buffer_init(&rbuff, rarr, sizeof(rarr));
    return 0;
}

uint8_t audio_stream_create_frame(uint32_t length)
{
    static uint32_t space;
    space = rign_buffer_available_space(&rbuff);
	if(space > length + 4)
	{
		uint8_t *p = (uint8_t*)&length;
		rign_buffer_write(&rbuff, p[0]);
		rign_buffer_write(&rbuff, p[1]);
		rign_buffer_write(&rbuff, p[2]);
		rign_buffer_write(&rbuff, p[3]);
		w_length = length;
		w_cnt = 0;
		if(cnt_frame + length > cnt_frame)
		{
			cnt_frame += length;
		}
		return 1;
	}
	w_length = 0;
	return 0;
}

void audio_stream_write_frame(uint8_t data)
{
	if(w_cnt < w_length)
	{
		w_cnt++;
		rign_buffer_write(&rbuff, data);
	}
}

uint32_t audio_stream_length(void)
{
	if(rign_buffer_available_byte(&rbuff) > 3)
	{
		uint32_t length;
		uint8_t *p = (uint8_t*)&length;
		p[0] = rign_buffer_read_offset(&rbuff, 0);
		p[1] = rign_buffer_read_offset(&rbuff, 1);
		p[2] = rign_buffer_read_offset(&rbuff, 2);
		p[3] = rign_buffer_read_offset(&rbuff, 3);
		
		return length;
	}
	return 0;
}

uint8_t audio_stream_read(void)
{
	return rign_buffer_read_u8(&rbuff);
}

uint8_t audio_stream_frame_is_complite(void)
{
    
    uint32_t lenframe = audio_stream_length();
    if(lenframe == 0)
        return 0;
    if(lenframe > 4000)
    {
        asm("nop");
    }
    uint32_t len = rign_buffer_available_byte(&rbuff);
    return len > (lenframe + 4) ? 1 : 0;
}

uint8_t audio_stream_is_ready(void)
{
    //static uint32_t space = 0;
    //space = rign_buffer_available_space(&rbuff);
    //printf("%u: %u\n", space, cnt_frame);
	return cnt_frame > 1024 * 10 ? 1 : 0;
}
