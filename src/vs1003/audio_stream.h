#ifndef AUDIO_STREAM_H
#define AUDIO_STREAM_H

#include "stdint.h"


uint8_t audio_stream_init(void);
uint8_t audio_stream_create_frame(uint32_t length);
void audio_stream_write_frame(uint8_t data);

uint32_t audio_stream_length(void);
uint8_t audio_stream_read(void);

uint8_t audio_stream_frame_is_complite(void);
uint8_t audio_stream_is_ready(void);

#endif