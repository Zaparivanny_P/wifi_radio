/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2014        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */
//#include "usbdisk.h"	/* Example: Header file of existing USB MSD control module */
//#include "atadrive.h"	/* Example: Header file of existing ATA harddisk control module */
//#include "sdcard.h"		/* Example: Header file of existing MMC/SDC contorl module */
#include "sdio_sd.h"

/* Definitions of physical drive number for each drive */
#define ATA		0	/* Example: Map ATA harddisk to physical drive 0 */
#define MMC		1	/* Example: Map MMC/SD card to physical drive 1 */
#define USB		2	/* Example: Map USB MSD to physical drive 2 */


/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	if (pdrv) return STA_NOINIT;	
	//SDCardState cardstate = SD_GetState();
	
	//if(cardstate == SD_OK) return 0;
    return 0;
	//return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
	if (pdrv) return STA_NOINIT;
    if(SD_Init() == SD_OK) return 0;
	
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Sector address in LBA */
	UINT count		/* Number of sectors to read */
)
{

	if (pdrv || !count) return RES_PARERR;
	
	SD_ReadMultiBlocks(buff, sector*512, 512, count);
    SD_WaitReadOperation();
    while(SD_GetStatus() != SD_TRANSFER_OK);

	return RES_OK;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Sector address in LBA */
	UINT count			/* Number of sectors to write */
)
{
    SD_WriteMultiBlocks((uint8_t*)buff, sector * 512, 512, count);
    SD_WaitReadOperation();
    while(SD_GetStatus() != SD_TRANSFER_OK);


	return RES_OK;
}
#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
    extern SD_CardInfo SDCardInfo;
    switch (cmd)
    {
    /* Flush disk cache (for write functions) */
    case CTRL_SYNC:
        return RES_OK;

    /* Get media size (for only f_mkfs()) */
    case GET_SECTOR_COUNT:
        *(DWORD*)buff = SDCardInfo.CardCapacity/SDCardInfo.CardBlockSize;
        return RES_OK;

    /* Get sector size (for multiple sector size (_MAX_SS >= 1024)) */
    case GET_SECTOR_SIZE:
        *(WORD*)buff = SDCardInfo.CardBlockSize;
        return RES_OK;

    /* Get erase block size (for only f_mkfs()) */
    case GET_BLOCK_SIZE:
        *(WORD*)buff = 1;
        return RES_OK;
    }
    return RES_PARERR;
}
#endif
