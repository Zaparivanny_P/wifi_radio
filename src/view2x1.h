#ifndef VIEW2X1_H
#define VIEW2X1_H

#include "ws2812.h"


struct View_color
{
	float r;
	float g;
	float b;
};

void view_off();
void view_disp_path(uint8_t* path);
uint8_t veiw_disp_playlist(uint8_t mode);
uint8_t view_volume_control(uint8_t vol);

void view_tick();

uint8_t view_processed(void);

#endif