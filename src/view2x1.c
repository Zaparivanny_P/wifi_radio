#include "view2x1.h"

#include "math.h"
#include "stdio.h"
#include "string.h"

struct View_color vw_led1,  vw_led2;
struct View_color  vw_ledk1, vw_ledk2;
uint8_t view_cnt, wv_tick_cnt;

float _view_calc_k(float v1, float v2, uint8_t n)
{
	float dist = (v2 - v1) / n;
	return dist;
}

void _view_calc_ledk(struct View_color *led, struct View_color *cl, struct View_color *ledk, uint8_t n)
{
	ledk->r = _view_calc_k(led->r, cl->r, n);
	ledk->g = _view_calc_k(led->g, cl->g, n);
	ledk->b = _view_calc_k(led->b, cl->b, n);
}

void view_off()
{
	struct View_color vw_cl1;
	vw_cl1.r = 0;
	vw_cl1.g = 0;
	vw_cl1.b = 0;
	
	uint8_t n = 60;
	
	_view_calc_ledk(&vw_led1, &vw_cl1, &vw_ledk1, n);
	_view_calc_ledk(&vw_led2, &vw_cl1, &vw_ledk2, n);
	wv_tick_cnt = 0xFF;
	view_cnt = n;
	wv_tick_cnt = 0;
}

uint8_t _view_parse(struct View_color *cl, uint8_t* path)
{
	uint8_t s = strlen((const char*)path);
	uint8_t mode = 0;
	uint8_t index_begin, index_end;
	for(uint8_t i = s; i > 0; i--)
	{
		switch(mode)
		{
			case 0:
				if(path[i] != ']')
					break;
				mode = 1;
				index_end = i;
				break;
				
			case 1:
				if(path[i] != '[')
					break;
				mode = 2;
				index_begin = i;
				break;
			case 2:
            {
                static uint8_t r, g, b;
				sscanf((const char*)(path + index_begin + 1), "%i,%i,%i", &r, &g, &b);
                cl->r = r;
                cl->g = g;
                cl->b = b;
                return 0;
            }
		}
	}
	return -1;
}

void view_disp_path(uint8_t* path)
{
	struct View_color vw_cl1;
	struct View_color vw_cl2;
	
	uint32_t vcrc;
    if(path != NULL)
    {
        CRC_ResetDR();
        vcrc = CRC_CalcBlockCRC((uint32_t*)path, strlen((const char*)path));
    }
    else
        vcrc = 0;
	
    vw_cl1.r = 20;
    vw_cl1.g = 0;
    vw_cl1.b = 0;
	
	vw_cl2.r = ((vcrc >> 0) & 0xFF) / 9;
    vw_cl2.g = ((vcrc >> 8) & 0xFF) / 9;
    vw_cl2.b = ((vcrc >> 16) & 0xFF) / 9;
    
    if(path != (void*)0)
        _view_parse(&vw_cl2, path);
	
	uint8_t n = 30;
	
	_view_calc_ledk(&vw_led1, &vw_cl1, &vw_ledk1, n);
	_view_calc_ledk(&vw_led2, &vw_cl2, &vw_ledk2, n);
	
	wv_tick_cnt = 0xFF;
	view_cnt = n;
	wv_tick_cnt = 0;
}

uint8_t veiw_disp_playlist(uint8_t mode)
{
	struct View_color vw_cl1;
	struct View_color vw_cl2;
	
    vw_cl1.r = 0;
    vw_cl1.g = 20;
    vw_cl1.b = 0;
	
	switch(mode)
	{
		case 1:
			vw_cl2.r = 20;
			vw_cl2.g = 20;
			vw_cl2.b = 0;
			break;
		case 2:
			vw_cl2.r = 0;
			vw_cl2.g = 20;
			vw_cl2.b = 10;
			break;
			
		default:
		
			break;
	}
    

	uint8_t n = 30;
	
	_view_calc_ledk(&vw_led1, &vw_cl1, &vw_ledk1, n);
	_view_calc_ledk(&vw_led2, &vw_cl2, &vw_ledk2, n);
	
	wv_tick_cnt = 0xFF;
	view_cnt = n;
	wv_tick_cnt = 0;
    return 0;
}


uint8_t view_volume_control(uint8_t vol)
{
	struct View_color vw_cl1;
	struct View_color vw_cl2;
	
    vw_cl1.r = 0;
    vw_cl1.g = 0;
    vw_cl1.b = 20;

	vw_cl2.r = (0xFF - ((uint16_t)vol * (uint16_t)vol / 256)) / 30;
	vw_cl2.g = 0;
	vw_cl2.b = ((uint16_t)vol * (uint16_t)vol / 256) / 2;

	uint8_t n = 30;
	
	_view_calc_ledk(&vw_led1, &vw_cl1, &vw_ledk1, n);
	_view_calc_ledk(&vw_led2, &vw_cl2, &vw_ledk2, n);
	
	wv_tick_cnt = 0xFF;
	view_cnt = n;
	wv_tick_cnt = 0;
    return 0;
}


void _view_calc_smooth(struct View_color *led, struct View_color *ledk)
{
	led->r += ledk->r;
	led->g += ledk->g;
	led->b += ledk->b;
}

void _view_to_ws(struct WS2812_color *src, struct View_color *dst)
{
	src->red = (uint8_t)dst->r;
	src->green = (uint8_t)dst->g;
	src->blue = (uint8_t)dst->b;
}

uint8_t view_processed(void)
{
    if(wv_tick_cnt < view_cnt)
    {
        return -1;
    }
    return 0;
}

void view_tick()
{
	//static uint8_t _red = 0, _green = 0, _blue = 0;
    /*static uint8_t freq = 0;
    if(++freq < 5)
    {
        return;
    }
    freq = 0;*/
	if(wv_tick_cnt < view_cnt)
	{
		struct WS2812_color c1, c2;
		wv_tick_cnt++;
		_view_calc_smooth(&vw_led1, &vw_ledk1);
		_view_calc_smooth(&vw_led2, &vw_ledk2);
		_view_to_ws(&c1, &vw_led1);
		_view_to_ws(&c2, &vw_led2);
		WS2812_send_color(&c1);
		WS2812_send_color(&c2);
	}
	
}